type AppPath = '' | 'auth';
type AppPathKey = 'MAIN' | 'AUTH';

export const AppRouteRecord: Record<AppPathKey, AppPath> = {
    MAIN: '',
    AUTH: 'auth',
}