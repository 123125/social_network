import { LoginFormModel } from '../login/login-form.model';

export class RegisterFormModel extends LoginFormModel {
    email: string | null = null;
}
