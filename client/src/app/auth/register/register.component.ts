import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { StrongPasswordPattern, ValidEmailPattern } from '../../app.config';
import { CleanSubscriptions } from '../../helpers/clean-subscriptions.class';
import { AuthService } from '../auth.service';
import { RegisterFormModel } from './register-form.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends CleanSubscriptions implements OnInit {
  public formGroup: UntypedFormGroup = this.formBuilder.group(new RegisterFormModel());

  // controls
  get emailControl(): UntypedFormControl {
    return this.formGroup.controls.email as UntypedFormControl;
  }
  get loginControl(): UntypedFormControl {
    return this.formGroup.controls.login as UntypedFormControl;
  }
  get passwordControl(): UntypedFormControl {
    return this.formGroup.controls.password as UntypedFormControl;
  }

  constructor(
    private readonly formBuilder: UntypedFormBuilder,
    private readonly authService: AuthService
  ) {
    super();
  }

  ngOnInit(): void {
    this.formGroup.controls.email.setValidators([
      Validators.required,
      Validators.pattern(ValidEmailPattern)
    ]);
    this.loginControl.setValidators([
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(20)
    ]);
    this.passwordControl.setValidators([
      Validators.required,
      Validators.minLength(4),
      Validators.pattern(StrongPasswordPattern)
    ]);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  public submit(): void {
    const data = this.formGroup.value;
    this.addSubscription(
      this.authService.register$(data, this.formGroup).subscribe()
    );
  }

}
