import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import jwt_decode from 'jwt-decode';

import { ApiHttpEntity } from '../helpers/api-http-base.class';
import { BrowserStorageService, StorageKey } from '../helpers/services/browser-storage.service';
import { LoginFormModel } from './login/login-form.model';
import { RegisterFormModel } from './register/register-form.model';
import { ResetPasswordEmailModel } from './reset-password-email/reset-password.model';
import { ResetPassword } from './reset-password/reset-password.model';
import { AppRouteRecord } from '../app-routing';
import { AuthRouteRecord } from './auth-routing';
import { User } from './auth.model';

type AuthPathName = 'LOGIN' | 'REGISTER' | 'SEND_RESET_PASSWORD_EMAIL' | 'RESET_PASSWORD';

const AuthPath: Record<AuthPathName, string> = {
    LOGIN: 'users/login',
    REGISTER: 'users/register',
    SEND_RESET_PASSWORD_EMAIL: 'users/request-reset-password',
    RESET_PASSWORD: 'users/reset-password',
};

type AuthResponse = { token: string };

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private loginHttp: ApiHttpEntity<LoginFormModel, AuthResponse>;
    private registerHttp: ApiHttpEntity<LoginFormModel, AuthResponse>;
    private sendResetPasswordEmailHttp: ApiHttpEntity<ResetPasswordEmailModel, void>;
    private resetPasswordHttp: ApiHttpEntity<ResetPassword, void>;

    constructor(
        private readonly http: HttpClient,
        private readonly router: Router,
        private readonly browserStorageService: BrowserStorageService,
    ) {
        this.loginHttp = new ApiHttpEntity({
            post: AuthPath.LOGIN
        }, this.http);

        this.registerHttp = new ApiHttpEntity({
            post: AuthPath.REGISTER
        }, this.http);

        this.sendResetPasswordEmailHttp = new ApiHttpEntity({
            post: AuthPath.SEND_RESET_PASSWORD_EMAIL
        }, this.http);

        this.resetPasswordHttp = new ApiHttpEntity({
            post: AuthPath.RESET_PASSWORD
        }, this.http)
    }

    public login$(body: LoginFormModel, formGroup: UntypedFormGroup): Observable<string> {
        return this.loginHttp.post$(body, formGroup)
            .pipe(
                map((response: AuthResponse) => response.token),
                tap((token: string) => this.proceedAuth(token))
            );
    }

    public register$(body: RegisterFormModel, formGroup: UntypedFormGroup): Observable<string> {
        return this.registerHttp.post$(body, formGroup)
            .pipe(
                map((response: AuthResponse) => response.token),
                tap((token: string) => this.proceedAuth(token))
            );
    }

    public sendResetPasswordEmail$(body: ResetPasswordEmailModel, formGroup: UntypedFormGroup): Observable<void> {
        return this.sendResetPasswordEmailHttp.post$(body, formGroup);
    }

    public resetPassword$(body: ResetPassword, formGroup: UntypedFormGroup): Observable<void> {
        return this.resetPasswordHttp.post$(body, formGroup);
    }

    public logout(): void {
        this.browserStorageService.removeFromLocalStorage(StorageKey.TOKEN);
        this.browserStorageService.removeFromLocalStorage(StorageKey.USER);
        this.router.navigateByUrl(`${AppRouteRecord.AUTH}/${AuthRouteRecord.LOGIN}`);
    }

    public getUserFromStorage(): string | undefined {
        return this.browserStorageService.getFromStorage(StorageKey.USER);
    }

    public checkAuthForGuard$(): Observable<boolean> {
        const token = this.browserStorageService.getFromStorage(StorageKey.TOKEN);
        const isExist = token !== undefined;
        return of(isExist);
    }

    private saveUserToStorage(jwtToken?: string): void {
        const token = jwtToken ?? this.browserStorageService.getFromStorage(StorageKey.TOKEN) ?? undefined;
        const decoded = (token !== undefined) ? jwt_decode(token) : null;
        this.browserStorageService.addToLocalStorage(StorageKey.USER, decoded);
    }

    private proceedAuth(token: string): void {
        this.saveUserToStorage(token);
        this.browserStorageService.addToLocalStorage(StorageKey.TOKEN, token);
        this.router.navigateByUrl(AppRouteRecord.MAIN);
    }

}
