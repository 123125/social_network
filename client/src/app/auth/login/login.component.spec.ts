import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder, UntypedFormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { LoginComponent } from './login.component';
import { of } from 'rxjs';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let formBuilder: UntypedFormBuilder;
  let authService: AuthService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [ LoginComponent ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    formBuilder = TestBed.inject(UntypedFormBuilder);
    component.ngOnInit();

    authService = TestBed.inject(AuthService);
    spyOn(authService, 'login$').and.returnValue(of());

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('loginControl getter should return login control of formGroup', () => {
    expect(component.loginControl).toEqual(component['formGroup'].controls.login as UntypedFormControl);
  });

  it('paaswordControl getter should return password control of formGroup', () => {
    expect(component.passwordControl).toEqual(component['formGroup'].controls.password as UntypedFormControl);
  });

  describe('login control', () => {
    it('should be invalid if input is empty', () => {
      component['formGroup'].controls.login.setValue('');
      expect(component['formGroup'].controls.login.invalid).toBeTrue();
    });
  
    it('should be invalid if input value length is less than 4', () => {
      component['formGroup'].controls.login.setValue('123');
      expect(component['formGroup'].controls.login.invalid).toBeTrue();
    });
  
    it('should be invalid if input value length is more than 20', () => {
      component['formGroup'].controls.login.setValue('123456789012345678901');
      expect(component['formGroup'].controls.login.invalid).toBeTrue();
    });
  
    it('should be valid if input is not empty', () => {
      component['formGroup'].controls.login.setValue('testLogin');
      expect(component['formGroup'].controls.login.valid).toBeTrue();
    });
  });

  describe('password control', () => {
    it('should be invalid if input is empty', () => {
      component['formGroup'].controls.password.setValue('');
      expect(component['formGroup'].controls.password.invalid).toBeTrue();
    });
  
    it('should be invalid if input value length is less than 4', () => {
      component['formGroup'].controls.password.setValue('123');
      expect(component['formGroup'].controls.password.invalid).toBeTrue();
    });
  
    it('should be invalid if password is not strong', () => {
      component['formGroup'].controls.password.setValue('anypassword');
      expect(component['formGroup'].controls.password.invalid).toBeTrue();
    });
  
    it('should be valid if input is not empty and password is strong', () => {
      component['formGroup'].controls.password.setValue('78ui&*UI');
      expect(component['formGroup'].controls.password.valid).toBeTrue();
    });
  });

  it('submit should call login$ of authService', () => {
    component.submit();
    expect(authService.login$).toHaveBeenCalled();
  });
});
