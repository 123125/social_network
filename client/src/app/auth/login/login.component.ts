import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { StrongPasswordPattern } from 'src/app/app.config';
import { CleanSubscriptions } from '../../helpers/clean-subscriptions.class';
import { AuthService } from '../auth.service';
import { LoginFormModel } from './login-form.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends CleanSubscriptions implements OnInit {
  public formGroup: UntypedFormGroup = this.formBuilder.group(new LoginFormModel());

  // controls
  get loginControl(): UntypedFormControl {
    return this.formGroup.controls.login as UntypedFormControl;
  }
  get passwordControl(): UntypedFormControl {
    return this.formGroup.controls.password as UntypedFormControl;
  }

  constructor(
    private readonly formBuilder: UntypedFormBuilder,
    private readonly authService: AuthService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.loginControl.setValidators([
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(20),
    ]);
    this.passwordControl.setValidators([
      Validators.required,
      Validators.minLength(4),
      Validators.pattern(StrongPasswordPattern)
    ]);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  public submit(): void {
    const data = this.formGroup.value;
    this.addSubscription(
      this.authService.login$(data, this.formGroup).subscribe()
    );
  }

}
