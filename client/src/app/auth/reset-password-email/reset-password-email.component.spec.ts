import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder, UntypedFormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { of } from 'rxjs';
import { ResetPasswordEmailComponent } from './reset-password-email.component';
import { ResetPasswordEmailModel } from './reset-password.model';

describe('ResetPasswordEmailComponent', () => {
  let component: ResetPasswordEmailComponent;
  let fixture: ComponentFixture<ResetPasswordEmailComponent>;
  let formBuilder: UntypedFormBuilder;
  let authService: AuthService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [ ResetPasswordEmailComponent ],
    }).compileComponents();

    fixture = TestBed.createComponent(ResetPasswordEmailComponent);
    component = fixture.componentInstance;
    formBuilder = TestBed.inject(UntypedFormBuilder);
    component.ngOnInit();

    authService = TestBed.inject(AuthService);
    spyOn(authService, 'sendResetPasswordEmail$').and.returnValue(of());

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('emailControl getter should return email control of formGroup', () => {
    expect(component.emailControl).toEqual(component['formGroup'].controls.email as UntypedFormControl);
  });

  describe('email control', () => {
    it('should be invalid if input is empty', () => {
      component['formGroup'].controls.email.setValue('');
      expect(component['formGroup'].controls.email.invalid).toBeTrue();
    });
  
    it('should be invalid if input value is invalid', () => {
      const emails = ['12321312', 'test@', 'someemail', 'test@test'];
      emails.forEach(email => {
        component['formGroup'].controls.email.setValue(email);
        expect(component['formGroup'].controls.email.invalid).toBeTrue();
      });
    });
  
    it('should be valid if input is not empty and email is correct', () => {
      component['formGroup'].controls.email.setValue('test@gmail.com');
      expect(component['formGroup'].controls.email.valid).toBeTrue();
    });
  });

  describe('submit', () => {
    it('should call sendResetPasswordEmail$ of authService', () => {
      component.submit();
      expect(authService.sendResetPasswordEmail$).toHaveBeenCalled();
    });

    it('should call isEmailSent$.next with true', () => {
      const email = '123@gmail.com';
      const resetPasswordEmail = new ResetPasswordEmailModel();
      resetPasswordEmail.email = email;
      spyOn(component.isEmailSent$, 'next');
      authService.sendResetPasswordEmail$(resetPasswordEmail, formBuilder.group({email})).subscribe(() => {
        expect(component.isEmailSent$.next).toHaveBeenCalledOnceWith(true);
      }).unsubscribe();
    });
  });
});
