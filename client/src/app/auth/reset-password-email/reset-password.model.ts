type ResetPasswordEmail = {
    email: string | null;
};

export class ResetPasswordEmailModel implements ResetPasswordEmail {
    email: string | null = null;
}
