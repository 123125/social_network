import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { ValidEmailPattern } from 'src/app/app.config';
import { CleanSubscriptions } from 'src/app/helpers/clean-subscriptions.class';
import { AuthService } from '../auth.service';
import { ResetPasswordEmailModel } from './reset-password.model';

@Component({
    selector: 'app-reset-password-email',
    templateUrl: 'reset-password-email.component.html',
    styleUrls: ['reset-password-email.component.scss'],
})
export class ResetPasswordEmailComponent extends CleanSubscriptions implements OnInit {
    public formGroup: UntypedFormGroup = this.formBuilder.group(new ResetPasswordEmailModel());

    public isEmailSent$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    get emailControl(): UntypedFormControl {
        return this.formGroup.get('email') as UntypedFormControl;
    }

    constructor(
        private readonly formBuilder: UntypedFormBuilder,
        private readonly authService: AuthService,
    ) {
        super();
    }

    ngOnInit(): void {
        this.formGroup.controls.email.setValidators([
            Validators.required,
            Validators.pattern(ValidEmailPattern),
        ]);
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public submit(): void {
        const data: ResetPasswordEmailModel = this.formGroup.value;
        this.addSubscription(
            this.authService.sendResetPasswordEmail$(data, this.formGroup).subscribe(() => {
                this.isEmailSent$.next(true);
            })
        );
    }
}
