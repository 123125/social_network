export type ResetPassword = {
    email: string;
    password: string;
    token: string;
}

export class ResetPasswordModel {
    password: string | null = null;
    confirmPassword: string | null = null;    
}