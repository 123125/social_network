import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { UntypedFormBuilder, UntypedFormControl, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";
import { AuthService } from "../auth.service";
import { ResetPasswordComponent } from "./reset-password.component";
import { of } from 'rxjs';
import { Router } from "@angular/router";
import { AppRouteRecord } from "src/app/app-routing";
import { AuthRouteRecord } from "../auth-routing";


describe('ResetPasswordComponent', () => {
    let component: ResetPasswordComponent;
    let fixture: ComponentFixture<ResetPasswordComponent>;
    let formBuilder: UntypedFormBuilder;
    let authService: AuthService;
    let router: Router;
  
    beforeEach(async () => {
      await TestBed.configureTestingModule({
        imports: [
          ReactiveFormsModule,
          FormsModule,
          HttpClientTestingModule,
          RouterTestingModule
        ],
        declarations: [ ResetPasswordComponent ],
      })
      .compileComponents();
  
      fixture = TestBed.createComponent(ResetPasswordComponent);
      component = fixture.componentInstance;
      formBuilder = TestBed.inject(UntypedFormBuilder);
      component.ngOnInit();
      router = TestBed.inject(Router);

  
      authService = TestBed.inject(AuthService);
      spyOn(authService, 'resetPassword$').and.returnValue(of());
  
      fixture.detectChanges();
    });
  
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('passwordControl getter should return password control of formGroup', () => {
      expect(component.passwordControl).toEqual(component['formGroup'].controls.password as UntypedFormControl);
    });

    it('confirmPasswordControl getter should return password confirm control of formGroup', () => {
      expect(component.confirmPasswordControl).toEqual(component['formGroup'].controls.confirmPassword as UntypedFormControl);
    });

    describe('password control', () => {
      it('should be invalid if input is empty', () => {
        component['formGroup'].controls.password.setValue('');
        expect(component['formGroup'].controls.password.invalid).toBeTrue();
      });
  
      it('should be invalid if input value length is less than 4', () => {
        component['formGroup'].controls.password.setValue('123');
        expect(component['formGroup'].controls.password.invalid).toBeTrue();
      });
  
      it('should be invalid if password is not strong', () => {
        component['formGroup'].controls.password.setValue('anypassword');
        expect(component['formGroup'].controls.password.invalid).toBeTrue();
      });
  
      it('should be valid if input is not empty and password is strong', () => {
        component['formGroup'].controls.password.setValue('78ui&*UI');
        expect(component['formGroup'].controls.password.valid).toBeTrue();
      });
    });

    describe('confirm password control', () => {
      it('should be invalid if input is empty', () => {
        component['formGroup'].controls.confirmPassword.setValue('');
        expect(component['formGroup'].controls.confirmPassword.invalid).toBeTrue();
      });
  
      it('should be invalid if input value length is less than 4', () => {
        component['formGroup'].controls.confirmPassword.setValue('123');
        expect(component['formGroup'].controls.confirmPassword.invalid).toBeTrue();
      });
  
      it('should be invalid if confirm password is not strong', () => {
        component['formGroup'].controls.confirmPassword.setValue('anypassword');
        expect(component['formGroup'].controls.confirmPassword.invalid).toBeTrue();
      });

      it('should be invalid if confirm password is not equal to password', () => {
        component['formGroup'].controls.password.setValue('anypassword');
        component['formGroup'].controls.confirmPassword.setValue('78ui&*UI');
        expect(component['formGroup'].controls.confirmPassword.invalid).toBeTrue();
      });
  
      it('should be valid if input is not empty, equel to password value and password is strong', () => {
        component['formGroup'].controls.password.setValue('78ui&*UI');
        component['formGroup'].controls.confirmPassword.setValue('78ui&*UI');
        expect(component['formGroup'].controls.confirmPassword.valid).toBeTrue();
      });
    });

    describe('submit', () => {
      it('submit should call register$ of authService', () => {
        component.submit();
        expect(authService.resetPassword$).toHaveBeenCalled();
      });

      it('should call login$ of authService', () => {
        const resetPasswordBody = {
          email: '123@gmail.com',
          password: '78ui&*UI',
          token: 'sometoken',
        };
        spyOn(router, 'navigateByUrl')
        authService.resetPassword$(resetPasswordBody, formBuilder.group({})).subscribe(() => {
          expect(router.navigateByUrl).toHaveBeenCalledOnceWith(`${AppRouteRecord.AUTH}/${AuthRouteRecord.LOGIN}`);
        }).unsubscribe();
      });

    });

    
});