import { Component } from '@angular/core';
import { UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { StrongPasswordPattern } from 'src/app/app.config';
import { CleanSubscriptions } from 'src/app/helpers/clean-subscriptions.class';
import { MatchValidator } from 'src/app/helpers/validators/match-validator';
import { AuthService } from '../auth.service';
import { ResetPasswordModel } from './reset-password.model';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent extends CleanSubscriptions {
    public formGroup: UntypedFormGroup = this.formBuilder.group(new ResetPasswordModel());

    get passwordControl(): UntypedFormControl {
        return this.formGroup.controls.password as UntypedFormControl;
    }

    get confirmPasswordControl(): UntypedFormControl {
        return this.formGroup.controls.confirmPassword as UntypedFormControl;
    }

    constructor(
        private readonly formBuilder: UntypedFormBuilder,
        private readonly activatedRoute: ActivatedRoute,
        private readonly router: Router,
        private readonly authService: AuthService,
    ) {
        super();
    }

    ngOnInit(): void {
        this.passwordControl.setValidators([
            Validators.required,
            Validators.minLength(4),
            Validators.pattern(StrongPasswordPattern),
        ]);
        this.confirmPasswordControl.setValidators([
            Validators.required,
            Validators.minLength(4),
            Validators.pattern(StrongPasswordPattern),
            MatchValidator(
                this.passwordControl,
                this.confirmPasswordControl,
                'Confirm Password does not match with Password'
            ),
        ]);
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public submit(): void {
        const data = {
            email: this.activatedRoute.snapshot.queryParams.email,
            password: this.passwordControl.value,
            token: this.activatedRoute.snapshot.params.token,
        };
        this.addSubscription(
            this.authService.resetPassword$(data, this.formGroup).subscribe(
                () => this.router.navigateByUrl('/auth/login')
            )
        );
    }
}