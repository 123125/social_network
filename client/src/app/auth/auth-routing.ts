type AuthRoute = 'register' | 'login' | 'send-email' | 'reset-password';
type AuthRouteKey = 'REGISTER' | 'LOGIN' | 'SEND_EMAIL' | 'RESET_PASSWORD';

export const AuthRouteRecord: Record<AuthRouteKey, AuthRoute> = {
    REGISTER: 'register',
    LOGIN: 'login',
    SEND_EMAIL: 'send-email',
    RESET_PASSWORD: 'reset-password',
};