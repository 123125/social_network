import { HttpClientTestingModule } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { of } from 'rxjs';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';

describe('AuthGuard', () => {
    let injector: TestBed;
    let authService: AuthService
    let guard: AuthGuard;
    const routerMock = { navigateByUrl: jasmine.createSpy('navigateByUrl') };
    
    beforeEach(() => {
        TestBed.configureTestingModule({
          providers: [
            AuthGuard,
            { provide: Router, useValue: routerMock },
        ],
          imports: [HttpClientTestingModule]
        });
        injector = getTestBed();
        authService = injector.get(AuthService);
        guard = injector.get(AuthGuard);
    });

    describe('when checkAuthForGuard$ returns false', () => {
        beforeEach(() => {
            spyOn(authService, 'checkAuthForGuard$').and.returnValue(of(false));
        });

        it('should not redirect an authenticated user to the edit route', () => {
            guard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot).subscribe(() => {
                expect(routerMock.navigateByUrl).not.toHaveBeenCalled();
            }).unsubscribe();
        });
    });

    describe('when checkAuthForGuard$ returns true', () => {
        beforeEach(() => {
            spyOn(authService, 'checkAuthForGuard$').and.returnValue(of(true));
        });

        it('should redirect an authenticated user to the edit route', () => {
            guard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot).subscribe(() => {
                expect(routerMock.navigateByUrl).toHaveBeenCalledWith('/');
            }).unsubscribe();
        });
    });
});