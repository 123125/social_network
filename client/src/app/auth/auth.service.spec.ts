import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { UntypedFormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppRouteRecord } from '../app-routing';
import { AuthRouteRecord } from './auth-routing';
import { BrowserStorageService, StorageKey } from '../helpers/services/browser-storage.service';
import { AuthService } from './auth.service';
import { LoginComponent } from './login/login.component';

describe('AuthService', () => {
    const loginUser = {
        login: 'testUser',
        password: '78ui&*UI',
    };
    const registerUser = {
        ...loginUser,
        email: '123@gmail.com'
    }
    let authService: AuthService;
    let browserStorageService: BrowserStorageService;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                ReactiveFormsModule,
                FormsModule,
                HttpClientTestingModule,
                RouterTestingModule.withRoutes([{
                    path: `${AppRouteRecord.AUTH}/${AuthRouteRecord.LOGIN}`,
                    component: LoginComponent,
                }]),
            ],
            providers: [
                AuthService, 
            ]
        });
        authService = TestBed.inject(AuthService);
        browserStorageService = TestBed.inject(BrowserStorageService);
        router = TestBed.inject(Router);
    });


    it('should create', () => {
        expect(authService).toBeTruthy();
    });

    describe('login$', () => {
        it('login$ should call getFromStorage of browserStorageService with StorageKey.TOKEN', () => {
            spyOn(browserStorageService, 'getFromStorage').and.callThrough();
            authService.login$(loginUser, new UntypedFormGroup({})).subscribe(() => {
                expect(browserStorageService.getFromStorage).toHaveBeenCalledOnceWith(StorageKey.TOKEN);
            }).unsubscribe();
        });
    
        it('login$ should call addToLocalStorage of browserStorageService with StorageKey.USER', () => {
            spyOn(browserStorageService, 'addToLocalStorage').and.callThrough();
            authService.login$(loginUser, new UntypedFormGroup({})).subscribe(() => {
                expect(browserStorageService.addToLocalStorage).toHaveBeenCalledOnceWith(StorageKey.USER, loginUser);
            }).unsubscribe();
        });
    
        it('login$ should call addToLocalStorage of browserStorageService with StorageKey.Token', () => {
            spyOn(browserStorageService, 'addToLocalStorage').and.callThrough();
            authService.login$(loginUser, new UntypedFormGroup({})).subscribe(() => {
                expect(browserStorageService.addToLocalStorage).toHaveBeenCalledOnceWith(StorageKey.TOKEN, 'sometoken');
            }).unsubscribe();
        });
    
        it('login$ should call navigateByUrl of router with AppPath.MAIN', () => {
            spyOn(router, 'navigateByUrl').and.callThrough();
            authService.login$(loginUser, new UntypedFormGroup({})).subscribe(() => {
                expect(router.navigateByUrl).toHaveBeenCalledOnceWith(AppRouteRecord.MAIN);
            }).unsubscribe();
        });
    });

    describe('register$', () => {
        it('should call getFromStorage of browserStorageService with StorageKey.TOKEN', () => {
            spyOn(browserStorageService, 'getFromStorage').and.callThrough();
            authService.register$(registerUser, new UntypedFormGroup({})).subscribe(() => {
                expect(browserStorageService.getFromStorage).toHaveBeenCalledOnceWith(StorageKey.TOKEN);
            }).unsubscribe();
        });
    
        it('should call addToLocalStorage of browserStorageService with StorageKey.USER', () => {
            spyOn(browserStorageService, 'addToLocalStorage').and.callThrough();
            authService.register$(registerUser, new UntypedFormGroup({})).subscribe(() => {
                expect(browserStorageService.addToLocalStorage).toHaveBeenCalledOnceWith(StorageKey.USER, loginUser);
            }).unsubscribe();
        });
    
        it('should call addToLocalStorage of browserStorageService with StorageKey.Token', () => {
            spyOn(browserStorageService, 'addToLocalStorage').and.callThrough();
            authService.register$(registerUser, new UntypedFormGroup({})).subscribe(() => {
                expect(browserStorageService.addToLocalStorage).toHaveBeenCalledOnceWith(StorageKey.TOKEN, 'sometoken');
            }).unsubscribe();
        });
    
        it('should call navigateByUrl of router with AppPath.MAIN', () => {
            spyOn(router, 'navigateByUrl').and.callThrough();
            authService.register$(registerUser, new UntypedFormGroup({})).subscribe(() => {
                expect(router.navigateByUrl).toHaveBeenCalledOnceWith(AppRouteRecord.MAIN);
            }).unsubscribe();
        });
    });

    describe('logout', () => {
        it('should call removeFromLocalStorage method of BrowserStorageService two times', () => {
            spyOn(browserStorageService, 'removeFromLocalStorage').and.callThrough();
            authService.logout();
            expect(browserStorageService.removeFromLocalStorage).toHaveBeenCalledTimes(2);
        });
    
        it('should call removeFromLocalStorage method of BrowserStorageService with StorageKey.TOKEN argument', () => {
            spyOn(browserStorageService, 'removeFromLocalStorage').and.callThrough();
            authService.logout();
            expect(browserStorageService.removeFromLocalStorage).toHaveBeenCalledWith(StorageKey.TOKEN);
        });
    
        it('should call removeFromLocalStorage method of BrowserStorageService with StorageKey.USER argument', () => {
            spyOn(browserStorageService, 'removeFromLocalStorage').and.callThrough();
            authService.logout();
            expect(browserStorageService.removeFromLocalStorage).toHaveBeenCalledWith(StorageKey.USER);
        });
    
        it('should call nabigateByUrl method of router with AppPath.AUTH', () => {
            spyOn(router, 'navigateByUrl').and.callThrough();
            authService.logout();
            expect(router.navigateByUrl).toHaveBeenCalledOnceWith(`${AppRouteRecord.AUTH}/${AuthRouteRecord.LOGIN}`);
        });
    });

    describe('checkAuthForGuard$', () => {
        it('should call getFromStorage method of BrowserStorageService', () => {
            spyOn(browserStorageService, 'getFromStorage').and.returnValue('token');
            authService.checkAuthForGuard$();
            expect(browserStorageService.getFromStorage).toHaveBeenCalled();
        });
    
        it('should return true observable if getFromStorage of browserStorageService returns token', () => {
            spyOn(browserStorageService, 'getFromStorage').and.returnValue('token');
            authService.checkAuthForGuard$().subscribe(value => {
                expect(value).toBeTrue();
            }).unsubscribe();
        });
    
        it('should return true observable if getFromStorage of browserStorageService returns undefined', () => {
            spyOn(browserStorageService, 'getFromStorage').and.returnValue(undefined);
            authService.checkAuthForGuard$().subscribe(value => {
                expect(value).toBeFalse();
            }).unsubscribe();
        });    
    });
    
    it('getUserFromStorage should call getFromStorage method of BrowserStorageService', () => {
        spyOn(browserStorageService, 'getFromStorage').and.returnValue('token');
        authService.getUserFromStorage();
        expect(browserStorageService.getFromStorage).toHaveBeenCalled();
    });

});
