export type User = {
    email: string;
    iat: number;
    id: number;
    login: string;
    profileId: number;
}