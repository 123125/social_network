import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard  {
    constructor(
        private readonly router: Router,
        private readonly authService: AuthService,
    ) {}

    canActivate(
        _route: ActivatedRouteSnapshot,
        _state: RouterStateSnapshot
    ): Observable<boolean> {
        return this.authService.checkAuthForGuard$().pipe(
            tap((canActivate) => {
                if (canActivate) {
                    this.router.navigateByUrl('/');
                }
            }),
            map((canActivate) => !canActivate)
        );
    }
}
