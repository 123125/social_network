import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRouteRecord } from './app-routing';
import { AppComponent } from './app.component';
import { AuthGuard } from './auth/auth.guard';
import { MainGuard } from './main/main.guard';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      {
        path: AppRouteRecord.MAIN,
        loadChildren: () => import('./main/main.module').then(m => m.MainModule),
        canActivate: [MainGuard]
      },
      {
        path: AppRouteRecord.AUTH,
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
        canActivate: [AuthGuard]
      },
      {
        path: '**',
        redirectTo: AppRouteRecord.MAIN,
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
