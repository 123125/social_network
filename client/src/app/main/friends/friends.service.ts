import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { combineLatest } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { ApiHttpEntity } from '../../helpers/api-http-base.class';
import { IFriendModel } from './friend.model';

// TODO: move it to separate file
export enum FieldToCheck {
    PROFILE_ID = 'profileId',
    FRIEND_ID = 'friendId',
}

@Injectable({
    providedIn: 'root'
})
export class FriendsService {
    // ApiHttpEntities:
    private friendsListHttp: ApiHttpEntity<{ list: IFriendModel[], page: number }>;
    private yourRequestsListHttp: ApiHttpEntity<{ list: IFriendModel[], page: number }>;
    private friendRequestsListHttp: ApiHttpEntity<{ list: IFriendModel[], page: number }>;
    private friendHttp: ApiHttpEntity<{
        profileId?: number,
        friendId?: number,
    }, IFriendModel & {id: number}>;

    // loadings
    public friendsListHttpLoading$: Observable<boolean>;
    public yourRequestsListHttpLoading$: Observable<boolean>;
    public friendRequestsListHttpLoading$: Observable<boolean>;

    constructor(private readonly http: HttpClient) {
        // inits list https:
        this.friendsListHttp = new ApiHttpEntity({
            get: 'friends/list?profileId={profileId}',
        }, this.http);
        this.friendsListHttpLoading$ = this.friendsListHttp.loading$;

        this.yourRequestsListHttp = new ApiHttpEntity({
            get: 'friends/your-requests?profileId={profileId}',
        }, this.http);
        this.yourRequestsListHttpLoading$ = this.yourRequestsListHttp.loading$;

        this.friendRequestsListHttp = new ApiHttpEntity({
            get: 'friends/friends-requests?profileId={profileId}',
        }, this.http);
        this.friendRequestsListHttpLoading$ = this.friendRequestsListHttp.loading$;

        this.friendHttp = new ApiHttpEntity({
            post: 'friends/add',
            get: 'friends/{id}',
            delete: 'friends/delete/{id}',
            put: 'friends/activate/{id}',
        }, this.http);
    }

    /**
     * Gets all the friends' relations of the current user which are accepted
     * @param profileId profile id of the current user
     */
    public getFriends$(profileId: number): Observable<{ list: IFriendModel[] }> {
        return this.friendsListHttp.get$({
            profileId: Number(profileId),
        });
    }

    /**
     * Gets all the friends' requests of the current user which are not accepted
     * @param profileId profile id of the current user
     */
    public getYourRequests$(profileId: number): Observable<{ list: IFriendModel[] }> {
        return this.yourRequestsListHttp.get$({
            profileId: Number(profileId),
        });
    }

    /**
     * Gets all the friends' requests of the friends which are not accepted
     * @param profileId profile id of the current user
     */
    public getFriendRequests$(profileId: number): Observable<{ list: IFriendModel[] }> {
        return this.friendRequestsListHttp.get$({
            profileId: Number(profileId),
        });
    }

    /**
     * Gets a friend relation by its profile id
     * @param profileId id of the friend relation
     * @param field the field to compare with profileId
     */
    public getFriend$(id: number): Observable<IFriendModel> {
        return this.friendHttp.get$({id});
    }

    /**
     * Creates a new friend relation
     * @param profile profile of the current user
     * @param friend profile of the friend which the current user invited
     */
    public addFriend$(profileId: number, friendId: number): Observable<IFriendModel> {
        return this.friendHttp.post$({ profileId, friendId });
    }

    /**
     * Deletes friend relation
     * @param id identificator of friend relation to remove it
     */
    public deleteFriend$(id: number): Observable<void> {
        return this.friendHttp.delete$(id);
    }

    /**
     * Sets accepted field as true
     * @param friendRelation friend relation to make update
     */
    public acceptFriend(id: number): Observable<IFriendModel> {
        return this.friendHttp.put$(id, {});
    }
}
