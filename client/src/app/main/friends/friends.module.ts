import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared.module';
import { PageLoaderComponent } from '../components/page-loader/page-loader.component';
import { ProfileItemComponent } from '../components/profile-item/profile-item.component';
import { FriendsRoutingModule } from './friends-routing.module';
import { FriendsComponent } from './friends.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FriendsRoutingModule
    ],
    declarations: [
        FriendsComponent,
        ProfileItemComponent,
        PageLoaderComponent,
    ],
    providers: []
})
export class FriendsModule {}
