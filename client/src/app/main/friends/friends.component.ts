import { Component, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { combineLatest, of } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
import { debounceTime, distinctUntilChanged, map, share, startWith, switchMap, tap } from 'rxjs/operators';
import { CleanSubscriptions } from '../../helpers/clean-subscriptions.class';
import { ProfileItemModel } from '../components/profile-item/profile-item.class';
import { ProfileForm } from '../my-page/models/profile-form.model';
import { ProfileService } from '../my-page/profile/profile.service';
import { IFriendModel } from './friend.model';
import { FriendsService } from './friends.service';

export enum TabTypeEnum {
    FRIENDS_LIST = 'friendsList',
    PROFILES_SEARCH_LIST = 'profilesSearchList',
    FRIEND_REQUESTS_LIST = 'friendRequestsList',
    YOUR_REQUESTS_LIST = 'yourRequestsList',
}

@Component({
    selector: 'app-friends',
    templateUrl: 'friends.component.html',
    styleUrls: ['friends.component.scss'],
})
export class FriendsComponent extends CleanSubscriptions implements OnInit {
    // lists with ProfileItems
    friendsList$: Observable<ProfileItemModel[]> | undefined;
    yourRequestsList$: Observable<ProfileItemModel[]> | undefined;
    friendRequestsList$: Observable<ProfileItemModel[]> | undefined;
    profilesSearchList$: Observable<ProfileItemModel[]> | undefined;

    // control for input search
    profilesSearchControl = new UntypedFormControl('');

    // cariables for tabs
    selectedTab: TabTypeEnum = TabTypeEnum.FRIENDS_LIST;
    tabTypeEnum = TabTypeEnum;

    // general loading icluding all the lists
    public isLoading$: Observable<boolean> = combineLatest([
        this.friendsService.friendsListHttpLoading$,
        this.friendsService.yourRequestsListHttpLoading$,
        this.friendsService.friendRequestsListHttpLoading$,
    ]).pipe(
        map(([
            friendsListLoading,
            yourRequestsListLoading,
            friendRequestsListLoading,
        ]) => this.getLoadingAccordingToSelectedTab({
            friendsListLoading,
            yourRequestsListLoading,
            friendRequestsListLoading,
        })),
    );

    constructor(
        private readonly friendsService: FriendsService,
        private readonly profileService: ProfileService,
    ) {
        super();
    }

    ngOnInit(): void {
        // inits all list with profile items
        this.profileService.getMyProfile$().pipe(
            tap(profile => this.initFriendsLists(Number(profile.id)))
        ).subscribe();

        // add rxjs operators for profilesSearchControl
        const profilesSearchControl$ = this.profilesSearchControl.valueChanges.pipe(
            startWith(''),
            debounceTime(300),
            distinctUntilChanged()
        );

        // get profiles list when profilesSearchControl is entered
        this.profilesSearchList$ = profilesSearchControl$.pipe(
            switchMap((searchValue: string) => {
                if (searchValue.length === 0) {
                    return of({list: []});
                } else {
                    return this.profileService.getProfilesList$(searchValue);
                }
            }),
            map((profilesList) => profilesList.list),
            map((profiles) => this.mapProfileItemFromProfiles(profiles)),
        );
    }

    /**
     * Set all the lists with profileItems
     * @param profileId profile id of the current user
     */
    private initFriendsLists(profileId: number): void {
        this.friendsList$ = this.friendsService.getFriends$(profileId).pipe(
            share(),
            map((friendsResponse) => friendsResponse.list),
            map((relations) => this.mapProfileItemFromFriends(relations, profileId)),
        );

        this.yourRequestsList$ = this.friendsService.getYourRequests$(profileId).pipe(
            share(),
            map((friendsResponse) => friendsResponse.list),
            map((relations) => this.mapProfileItemFromFriends(relations, profileId)),
        );

        this.friendRequestsList$ = this.friendsService.getFriendRequests$(profileId).pipe(
            share(),
            map((friendsResponse) => friendsResponse.list),
            map((relations) => this.mapProfileItemFromFriends(relations, profileId)),
        );
    }

    /**
     * Maps friends array to profileItems array
     * @param profiles profiles array
     */
    private mapProfileItemFromFriends(relations: IFriendModel[], profileId: number): ProfileItemModel[] {
        return relations.map(relation => {
            const profileItem = new ProfileItemModel();
            if (profileId === relation.profileId && relation.friend !== undefined) {
                profileItem.profileId = relation.friendId.toString();
                profileItem.firstName = relation.friend.firstName;
                profileItem.lastName = relation.friend.lastName;
            } else {
                profileItem.profileId = relation.profileId.toString();
                profileItem.firstName = relation.profile.firstName;
                profileItem.lastName = relation.profile.lastName;
            }
            profileItem.id = relation.id.toString();
            if (relation.profile !== undefined) {

            }
            return profileItem;
        });
    }

    /**
     * Maps profiles array to profileItems array
     * @param profiles profiles array
     */
    private mapProfileItemFromProfiles(profiles: ProfileForm[]): ProfileItemModel[] {
        return profiles.map(profile => {
            const profileItem = new ProfileItemModel();
            profileItem.profileId = profile.id.toString();
            profileItem.firstName = profile.firstName;
            profileItem.lastName = profile.lastName;
            return profileItem;
        });
    }

    /**
     * Returns boolean for listed loadings
     * @param friendsListLoading loading of friendsList
     * @param yourRequestsListLoading loading of yourRequestsList
     * @param friendRequestsListLoading loading of friendRequestsList
     */
    private getLoadingAccordingToSelectedTab(loadings: {
        friendsListLoading: boolean,
        yourRequestsListLoading: boolean,
        friendRequestsListLoading: boolean,
    }): boolean {
        const { friendsListLoading, yourRequestsListLoading, friendRequestsListLoading } = loadings;
        switch (this.selectedTab) {
            case TabTypeEnum.FRIENDS_LIST:
                return friendsListLoading;

            case TabTypeEnum.YOUR_REQUESTS_LIST:
                return yourRequestsListLoading;

            case TabTypeEnum.FRIEND_REQUESTS_LIST:
                return friendRequestsListLoading;

            default:
                return false;
        }
    }

    /**
     * Removes friend relation
     * @param id identificator of friend relation to remove it
     */
    removeFriend(id: string): void {
        this.addSubscription(this.friendsService.deleteFriend$(Number(id)).subscribe((data) => console.log(11111, data)));
    }
}
