import { ProfileForm } from '../my-page/models/profile-form.model';

export interface IFriendModel {
    accepted: boolean;
    id: number;
    updatedAt: string;
    createdAt: string;
    friendId: number;
    profileId: number;
    profile: ProfileForm;
    friend: ProfileForm;
    isInvited: boolean;
}
