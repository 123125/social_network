import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main.component';
import { MyProfileGuard } from './my-page/my-profile.guard';

const routes: Routes = [
    {
        path: '',
        component: MainComponent,
        children: [
            {
                path: '',
                loadChildren: () => import('./my-page/my-page.module').then(m => m.MyPageModule),
            },
            {
                path: 'friends',
                loadChildren: () => import('./friends/friends.module').then(m => m.FriendsModule),
                canActivate: [MyProfileGuard],
            },
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MainRoutingModule { }
