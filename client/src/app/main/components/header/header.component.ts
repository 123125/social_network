import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.scss'],
})
export class HeaderComponent {
    @Output() logoutClick: EventEmitter<void> = new EventEmitter<void>();

    public logout(): void {
        this.logoutClick.emit();
    }
}
