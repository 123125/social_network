interface IPorfileItem {
    id?: string;
    firstName: string;
    lastName: string;
    avatarUrl: string;
    profileId: string;
    getFullName: () => string;
    isValid: () => boolean;
}

export class ProfileItemModel implements IPorfileItem {
    constructor(
        public id = '',
        public profileId = '',
        public firstName = '',
        public lastName = '',
        public avatarUrl = '',
    ) {}

    getFullName(): string {
        return this.firstName + ' ' + this.lastName;
    }

    isValid(): boolean {
        return this.firstName.length > 0 && this.lastName.length > 0;
    }
}
