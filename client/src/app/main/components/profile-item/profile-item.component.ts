import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { TabTypeEnum } from '../../friends/friends.component';
import { ProfileItemModel } from './profile-item.class';

@Component({
    selector: 'app-profile-item',
    templateUrl: 'profile-item.component.html',
    styleUrls: ['profile-item.component.scss']
})
export class ProfileItemComponent {
    @Input() set profileItem(value: ProfileItemModel) {
        this._profileItem = value;
    }

    get profileItem(): ProfileItemModel {
        if (this._profileItem === undefined) {
            throw Error('[ProfileItem] input data is undefined');
        }
        return this._profileItem;
    }

    @Input() isRemoveLink = true;
    @Input() itemType: TabTypeEnum | undefined;

    @Output() removeFriendEvent: EventEmitter<string> = new EventEmitter();

    get fullName(): string {
        const firstName = this.profileItem.firstName;
        const lastName = this.profileItem.lastName;
        return firstName + ' ' + lastName;
    }

    get linkId(): string {
        return this.profileItem.profileId;
    }

    get avatarUrl(): string {
        return this.profileItem.avatarUrl;
    }

    private _profileItem: ProfileItemModel | undefined;

    constructor(private readonly router: Router) {}

    goToProfile(): void {
        this.router.navigate(['profile', this.linkId], {
            queryParams: {
                relationId: this.profileItem.id,
            }
        });
    }

    removeFriend(): void {
        this.removeFriendEvent.emit(this.profileItem.id);
    }

}
