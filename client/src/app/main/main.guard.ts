import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppRouteRecord } from '../app-routing';
import { AuthService } from '../auth/auth.service';

@Injectable({
    providedIn: 'root'
})
export class MainGuard  {
    constructor(
        private readonly router: Router,
        private readonly authService: AuthService
    ) {}

    canActivate(
        _route: ActivatedRouteSnapshot,
        _state: RouterStateSnapshot
    ): Observable<boolean> {
        return this.authService.checkAuthForGuard$().pipe(
            tap((canActivate) => {
                if (!canActivate) {
                    this.router.navigateByUrl('/' + AppRouteRecord.AUTH);
                }
            })
        );
    }
}
