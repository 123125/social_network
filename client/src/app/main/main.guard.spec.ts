import { HttpClientTestingModule } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { of } from 'rxjs';
import { AppRouteRecord } from '../app-routing';
import { AuthService } from '../auth/auth.service';
import { MainGuard } from './main.guard';

describe('AuthGuard', () => {
    let injector: TestBed;
    let authService: AuthService
    let guard: MainGuard;
    const routerMock = { navigateByUrl: jasmine.createSpy('navigateByUrl') };
    
    beforeEach(() => {
        TestBed.configureTestingModule({
          providers: [
            MainGuard,
            { provide: Router, useValue: routerMock },
        ],
          imports: [HttpClientTestingModule]
        });
        injector = getTestBed();
        authService = injector.get(AuthService);
        guard = injector.get(MainGuard);
    });

    it('should redirect an unauthenticated user to the login route', () => {
        spyOn(authService, 'checkAuthForGuard$').and.returnValue(of(false));
        guard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot).subscribe(() => {
            expect(routerMock.navigateByUrl).toHaveBeenCalledWith('/' + AppRouteRecord.AUTH);
        }).unsubscribe();
    });
});