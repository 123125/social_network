import { Component, EventEmitter, Input, Output } from '@angular/core';
import { environment } from '../../../../../../environments/environment';

@Component({
    selector: 'app-profile-photo',
    templateUrl: 'profile-photo.component.html',
    styleUrls: ['profile-photo.component.scss']
})
export class ProfilePhotoComponent {
    @Input() imageSrc: string | undefined;
    @Input() isEditPhoto: boolean = false;
    @Output() uploadPhotoEvent: EventEmitter<File> = new EventEmitter();
    
    public baseUrl = environment.baseUrl

    uploadPhoto($event: any): void {
        const file: File = $event.target.files[0];
        this.uploadPhotoEvent.emit(file);
    }
}
