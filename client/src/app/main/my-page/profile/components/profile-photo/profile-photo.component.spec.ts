import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProfilePhotoComponent } from './profile-photo.component';

describe('ProfilePhotoComponent', () => {
    let component: ProfilePhotoComponent;
    let fixture: ComponentFixture<ProfilePhotoComponent>;
    let emitSpy: jasmine.Spy;
  
    beforeEach(async () => {
      await TestBed.configureTestingModule({
        imports: [
        ],
        declarations: [ ProfilePhotoComponent ],
      })
      .compileComponents();
  
      fixture = TestBed.createComponent(ProfilePhotoComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      emitSpy = spyOn(component.uploadPhotoEvent, 'emit');
    });
  
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should emit event on uploadPhotoEvent', () => {
        const mockFile = new File([''], 'filename', { type: 'text/html' });
        const mockEvt = { target: { files: [mockFile] } };
        component.uploadPhoto(mockEvt);
        expect(emitSpy).toHaveBeenCalled();
    });
});