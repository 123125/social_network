import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-profile-info',
    templateUrl: './profile-info.component.html',
    styleUrls: ['profile-info.component.scss'],
})
export class ProfileInfoComponent {
    // TODO: change type any
    @Input() data: any;
}
