import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { ApiHttpEntity } from '../../../helpers/api-http-base.class';
import { ProfileForm, ProfileResponseModel } from '../models/profile-form.model';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {
    private myProfileHttp: ApiHttpEntity<ProfileForm, ProfileResponseModel>;
    private myProfilePhotoHttp: ApiHttpEntity<FormData, {avatarUrl: string}>;
    private profileHttp: ApiHttpEntity<ProfileResponseModel>;
    private profilesListHttp: ApiHttpEntity<{ list: ProfileForm[], page: number }>;

    constructor(private readonly http: HttpClient) {
        this.myProfileHttp = new ApiHttpEntity({
            get: 'profiles/my',
            put: 'profiles/my',
            post: 'profiles/my',
        }, this.http);

        this.myProfilePhotoHttp = new ApiHttpEntity({
            post: 'profiles/my/photo',
            get: 'profiles/my/photo',
        }, this.http);

        this.profileHttp = new ApiHttpEntity({
            get: 'profiles/{id}',
        }, this.http);

        this.profilesListHttp = new ApiHttpEntity({
            get: 'profiles/list?take={take}&skip={skip}&search={search}',
        }, this.http);
    }

    public getMyProfile$(): Observable<ProfileResponseModel> {
        return this.myProfileHttp.get$();
    }

    public createMyProfile$(profile: ProfileForm, formGroup: UntypedFormGroup): Observable<ProfileResponseModel> {
        return this.myProfileHttp.post$(profile, formGroup);
    }

    public updateMyProfile$(profile: ProfileForm, formGroup: UntypedFormGroup): Observable<ProfileResponseModel> {
        if (profile.id === undefined) {
            throw Error('[ProfileService] id is undefined');
        }
        return this.myProfileHttp.put$(Number(profile.id), profile, formGroup);
    }

    public getProfile$(id: number): Observable<ProfileResponseModel> {
        return this.profileHttp.get$({id});
    }

    public getProfilesList$(search: string | undefined, take = 10, skip = 0): Observable<{ list: ProfileForm[] }> {
        return this.profilesListHttp.get$({
            take,
            skip,
            search
        });
    }

    public addProfilePhoto$(photo: File): Observable<{avatarUrl: string}> {
        const formData = new FormData();
        formData.append('photo', photo);
        this.myProfilePhotoHttp.setOptions({
            headers: new HttpHeaders(),
        });
        return this.myProfilePhotoHttp.post$(formData);
    }
}
