import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, Observable, Subject } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { CleanSubscriptions } from '../../../helpers/clean-subscriptions.class';
import { IFriendModel } from '../../friends/friend.model';
import { FriendsService } from '../../friends/friends.service';
import { ProfileForm, ProfileResponseModel } from '../models/profile-form.model';
import { ProfileService } from './profile.service';

@Component({
    selector: 'app-profile',
    templateUrl: 'profile.component.html',
    styleUrls: ['profile.component.scss']
})
export class ProfileComponent extends CleanSubscriptions implements OnInit {
    public myProfile$: Observable<ProfileResponseModel> | undefined;
    public profile$: Observable<ProfileResponseModel> | undefined;
    public profileImage: string | ArrayBuffer | null | undefined;

    private friendRelation: IFriendModel | undefined;

    private myProfileId: string | undefined;
    private profileId: string | undefined;
    private relationId: string | undefined;

    get isMyProfile(): boolean {
        return this.profileId === undefined;
    }

    get isAccepted(): boolean {
        return this.friendRelation !== undefined && this.friendRelation.accepted && this.friendRelation.isInvited;
    }

    get isInvited(): boolean {
        return this.friendRelation === undefined;
    }

    get isAddedByYou(): boolean {
        return this.friendRelation !== undefined
            && this.friendRelation.isInvited
            && this.myProfileId === this.friendRelation.profileId.toString();
    }

    constructor(
        private readonly profileService: ProfileService,
        private readonly friendsService: FriendsService,
        private readonly activatedRoute: ActivatedRoute,
        private readonly router: Router,
    ) {
        super();
    }

    ngOnInit(): void {
        // get ids form route
        this.profileId = this.activatedRoute.snapshot.params.id;
        this.relationId = this.activatedRoute.snapshot.queryParams.relationId;

        // init profile data
        this.initProfile();

        // reinit data if relation is created and user is invited
        this.addSubscription(
            this.activatedRoute.queryParams.subscribe(
                ({relationId}) => this.initProfile(relationId)
            )
        );

    }

    private initProfile(relationId = this.relationId): void {
        this.cleanSubscription();

        this.myProfile$ = this.profileService.getMyProfile$().pipe(
            tap((data) => this.myProfileId = data.id.toString())
        ) ?? undefined;

        if (this.profileId === undefined) {
            this.profile$ = this.myProfile$;
        } else {
            this.addSubscription(this.myProfile$.subscribe());
            this.profile$ = this.profileService.getProfile$(Number(this.profileId));
            if (relationId !== undefined) {
                this.addSubscription(
                    this.friendsService.getFriend$(Number(relationId))
                        .subscribe((data) => this.friendRelation = data ?? undefined)
                );
            }
        }
    }

    public addFriend(): void {
        if (this.profileId === undefined) {
            throw Error('[ProfileComponent] [addFriend] profileId is undefined');
        }
        this.addSubscription(
            this.profileService.getMyProfile$().pipe(
                switchMap((profile) => {
                    return this.friendsService.addFriend$(Number(profile.id), Number(this.profileId));
                })
            ).subscribe((data) => {
                this.router.navigate(['profile', this.profileId], {
                    queryParams: {
                        relationId: data.id,
                    },
                });
            })
        );
    }

    public acceptFriend(): void {
        if (this.friendRelation === undefined) {
            throw Error('[ProfileComponent] [acceptFriend] friendRelation is undefined');
        }
        this.addSubscription(
            this.friendsService.acceptFriend(this.friendRelation.id).subscribe()
        );
    }

    public uploadPhoto(file: File): void {
        if (this.profile$ === undefined) return;
        combineLatest(
            this.profile$,
            this.profileService.addProfilePhoto$(file)
        ).pipe(take(1)).subscribe(([profile, avatar]) => {
            this.profile$ = new Observable((observer) => (observer.next({
                ...profile,
                avatarUrl: avatar.avatarUrl
            })));
        });
    }

}
