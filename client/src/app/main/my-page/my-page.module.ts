import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared.module';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { MyPageRoutingModule } from './my-page-routing.module';
import { MyPageComponent } from './my-page.component';
import { ProfileInfoComponent } from './profile/components/profile-info/profile-info.component';
import { ProfilePhotoComponent } from './profile/components/profile-photo/profile-photo.component';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
    declarations: [
      MyPageComponent,
      ProfileComponent,
      ProfilePhotoComponent,
      ProfileInfoComponent,
      EditProfileComponent
    ],
    imports: [
      CommonModule,
      MyPageRoutingModule,
      SharedModule,
    ],
    providers: []
})
export class MyPageModule { }
