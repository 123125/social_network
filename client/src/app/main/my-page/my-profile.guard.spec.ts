import { HttpClientTestingModule } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { BrowserStorageService } from '../../helpers/services/browser-storage.service';
import { MyProfileGuard } from './my-profile.guard';

describe('MyProfileGuard', () => {
    let injector: TestBed;
    let browserStorageService: BrowserStorageService;
    let guard: MyProfileGuard;
    const routerMock = { navigateByUrl: jasmine.createSpy('navigateByUrl') };
    
    beforeEach(() => {
        TestBed.configureTestingModule({
          providers: [
            MyProfileGuard,
            { provide: Router, useValue: routerMock },
        ],
          imports: [HttpClientTestingModule]
        });
        injector = getTestBed();
        guard = injector.get(MyProfileGuard);
        browserStorageService = injector.get(BrowserStorageService);
    });

    describe('when there is user data', () => {
        beforeEach(() => {
            spyOn(browserStorageService, 'getFromStorage').and.returnValue({ profileId: '123'});
        });

        it('should not redirect an authenticated user to the edit route', () => {
            guard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot).subscribe(() => {
                expect(routerMock.navigateByUrl).not.toHaveBeenCalled();
            }).unsubscribe();
        });
    });

    describe('when there is no user data', () => {
        beforeEach(() => {
            spyOn(browserStorageService, 'getFromStorage').and.returnValue(null);
        });

        it('should redirect an authenticated user to the edit route', () => {
            guard.canActivate({} as ActivatedRouteSnapshot, {} as RouterStateSnapshot).subscribe(() => {
                expect(routerMock.navigateByUrl).toHaveBeenCalledWith('edit');
            }).unsubscribe();
        });
    });

});