export class ProfileForm {
    constructor(
        public firstName: string = '',
        public lastName: string = '',
        public gender: 'male' | 'female' = 'male',
        public language: string = '',
        public birthDate: string = '',
        public city: string = '',
        public phoneNumber: string = '',
        public description: string = '',
        public id: string = '',
    ) {}
};

export type ProfileResponseModel = {
    statusText: null,
    description: string,
    userId: number,
    firstName: string,
    lastName: string,
    phoneNumber: string,
    gender: 'male' | 'female',
    language: string,
    birthDate: string,
    city: string,
    deletedAt: null,
    id: number,
    status: 'offline' | 'online',
    createdAt: string,
    updatedAt: string,
    avatarUrl: string,
};