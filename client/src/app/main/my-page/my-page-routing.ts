type MyPageRoute = '' | 'edit' | 'send-email' | 'profile/:id';
type MyPageRouteKey = 'MY_PROFILE' | 'EDIT' | 'PROFILE';

export const MyPageRouteRecord: Record<MyPageRouteKey, MyPageRoute> = {
    MY_PROFILE: '',
    EDIT: 'edit',
    PROFILE: 'profile/:id',
}