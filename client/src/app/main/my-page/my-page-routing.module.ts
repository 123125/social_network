import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { MyPageRouteRecord } from './my-page-routing';
import { MyPageComponent } from './my-page.component';
import { MyProfileGuard } from './my-profile.guard';
import { ProfileComponent } from './profile/profile.component';

const routes = [
    {
        path: '',
        component: MyPageComponent,
        children: [
            {
                path: '',
                component: ProfileComponent,
                canActivate: [MyProfileGuard]
            },
            {
                path: MyPageRouteRecord.EDIT,
                component: EditProfileComponent
            },
            {
                path: 'profile/:id',
                component: ProfileComponent,
                canActivate: [MyProfileGuard]
            },
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MyPageRoutingModule { }
