import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { JwtData } from '../../helpers/models/jwt-data.model';
import { BrowserStorageService, StorageKey } from '../../helpers/services/browser-storage.service';

@Injectable({
    providedIn: 'root'
})
export class MyProfileGuard  {
    constructor(
        private readonly browserStorageService: BrowserStorageService,
        private readonly router: Router
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        const user = this.browserStorageService.getFromStorage<JwtData>(StorageKey.USER);
        return of(user).pipe(
            map(userData => Boolean(userData?.profileId)),
            tap(profileId => {
                if (!profileId) {
                    this.router.navigateByUrl('edit');
                }
            }),
        );
    }
}