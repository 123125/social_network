import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormBuilder, UntypedFormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, of, Subject } from 'rxjs';
import { JwtData } from '../../../helpers/models/jwt-data.model';
import { BrowserStorageService, StorageKey } from '../../../helpers/services/browser-storage.service';
import { ProfileForm } from '../models/profile-form.model';
import { ProfileService } from '../profile/profile.service';
import { EditProfileComponent } from './edit-profile.component';

describe('EditProfileComponent', () => {
    let component: EditProfileComponent;
    let fixture: ComponentFixture<EditProfileComponent>;
    let formBuilder: UntypedFormBuilder;
    let profileService: ProfileService;
    let browserStorageService: BrowserStorageService;
    let router: Router;
  
    beforeEach(async () => {
      await TestBed.configureTestingModule({
        imports: [
          ReactiveFormsModule,
          FormsModule,
          HttpClientTestingModule,
          RouterTestingModule
        ],
        declarations: [ EditProfileComponent ],
      })
      .compileComponents();
  
      fixture = TestBed.createComponent(EditProfileComponent);
      component = fixture.componentInstance;
      formBuilder = TestBed.inject(UntypedFormBuilder);
      component['isProfileExist'] = false;
      component.ngOnInit();

      profileService = TestBed.inject(ProfileService);
      spyOn(profileService, 'getMyProfile$').and.returnValue(of());
      spyOn(profileService, 'createMyProfile$').and.returnValue(of());
      spyOn(profileService, 'updateMyProfile$').and.returnValue(of());

      browserStorageService = TestBed.get(BrowserStorageService);
      spyOn(browserStorageService, 'getFromStorage').and.returnValue({});
      spyOn(browserStorageService, 'addToLocalStorage').and.returnValue();

      router = TestBed.inject(Router);

      fixture.detectChanges();
    });
  
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should call getMyProfile$ of profileService', () => {
        component['isProfileExist'] = true;
        component.ngOnInit();
        expect(profileService.getMyProfile$).toHaveBeenCalled();
    });

    it('should not call getMyProfile$ of profileService', () => {
        expect(profileService.getMyProfile$).not.toHaveBeenCalled();
    });

    describe('controls getters', () => {
        it('firstName getter should return firstName control of formGroup', () => {
            expect(component.firstName).toEqual(component['formGroup'].controls.firstName as UntypedFormControl);
        });

        it('lastName getter should return lastName control of formGroup', () => {
            expect(component.lastName).toEqual(component['formGroup'].controls.lastName as UntypedFormControl);
        });

        it('gender getter should return gender control of formGroup', () => {
            expect(component.gender).toEqual(component['formGroup'].controls.gender as UntypedFormControl);
        });

        it('language getter should return language control of formGroup', () => {
            expect(component.language).toEqual(component['formGroup'].controls.language as UntypedFormControl);
        });

        it('city getter should return city control of formGroup', () => {
            expect(component.city).toEqual(component['formGroup'].controls.city as UntypedFormControl);
        });

        it('phoneNumber getter should return phoneNumber control of formGroup', () => {
            expect(component.phoneNumber).toEqual(component['formGroup'].controls.phoneNumber as UntypedFormControl);
        });

        it('description getter should return description control of formGroup', () => {
            expect(component.description).toEqual(component['formGroup'].controls.description as UntypedFormControl);
        });
    });

    describe('submit', () => {
        it('should call createMyProfile$ of profileService', () => {
            component.submit();
            expect(profileService.createMyProfile$).toHaveBeenCalled();
        });

        it('should call updateMyProfile$ of profileService', () => {
            component['isProfileExist'] = true;
            component.submit();
            expect(profileService.updateMyProfile$).toHaveBeenCalled();
        });

        it('should navigate with empty url inside sibscribe of createMyProfile$', () => {
            const profileForm = new ProfileForm();
            spyOn(router, 'navigateByUrl');
            profileService.createMyProfile$(profileForm, formBuilder.group({})).subscribe(() => {
                expect(router.navigateByUrl).toHaveBeenCalledOnceWith('');
            }).unsubscribe();
        });

        it('should navigate with empty url inside sibscribe of updateMyProfile$', () => {
            const profileForm = new ProfileForm();
            spyOn(router, 'navigateByUrl');
            profileService.updateMyProfile$(profileForm, formBuilder.group({})).subscribe(() => {
                expect(router.navigateByUrl).toHaveBeenCalledOnceWith('');
            }).unsubscribe();
        });

        it('should call getFromStorage of browserStorageService inside subscribe of updateMyProfile$', () => {
            const profileForm = new ProfileForm();
            profileService.updateMyProfile$(profileForm, formBuilder.group({})).subscribe(() => {
                expect(browserStorageService.getFromStorage).toHaveBeenCalled();
            }).unsubscribe();
        });

        it('should call addToLocalStorage of browserStorageService inside subscribe of updateMyProfile$', () => {
            const profileForm = new ProfileForm();
            profileService.updateMyProfile$(profileForm, formBuilder.group({})).subscribe(() => {
                expect(browserStorageService.addToLocalStorage).toHaveBeenCalled();
            }).unsubscribe();
        });
    });

});