import { Component, OnInit } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ValidPhoneNumber } from '../../../app.config';
import { CleanSubscriptions } from '../../../helpers/clean-subscriptions.class';
import { ProfileForm } from '../models/profile-form.model';
import { ProfileService } from '../profile/profile.service';
import { BrowserStorageService, StorageKey } from '../../../helpers/services/browser-storage.service';
import { JwtData } from '../../../helpers/models/jwt-data.model';

@Component({
    selector: 'app-edit-profile',
    templateUrl: 'edit-profile.component.html',
    styleUrls: ['edit-profile.component.scss']
})
export class EditProfileComponent extends CleanSubscriptions implements OnInit {
    private formGroup: UntypedFormGroup = this.formBuilder.group(new ProfileForm());
    private isProfileExist = this.browserStorageService.getFromStorage<JwtData>(StorageKey.USER)?.profileId !== null;

    constructor(
        private readonly formBuilder: UntypedFormBuilder,
        private readonly profileService: ProfileService,
        private readonly router: Router,
        private readonly browserStorageService: BrowserStorageService
    ) {
        super();
    }

    ngOnInit(): void {
        this.setValidatators();
        if (this.isProfileExist) {
            this.addSubscription(
                this.profileService.getMyProfile$().subscribe((data) => {
                    this.firstName.setValue(data.firstName);
                    this.lastName.setValue(data.lastName);
                    this.gender.setValue(data.gender);
                    this.language.setValue(data.language);
                    this.birthDate.setValue(data.birthDate);
                    this.city.setValue(data.city);
                    this.phoneNumber.setValue(data.phoneNumber);
                    this.description.setValue(data.description);
                })
            );
        }
    }

    public submit(): void {
        const data = this.formGroup.value;
        if (this.isProfileExist) {
            this.addSubscription(
                this.profileService.updateMyProfile$(data, this.formGroup).subscribe(() => {
                    this.router.navigateByUrl('');
                })
            );
        } else {
            this.addSubscription(
                this.profileService.createMyProfile$(data, this.formGroup).subscribe((res) => {
                    const user = this.browserStorageService.getFromStorage<JwtData>(StorageKey.USER);
                    if (user) {
                        user.profileId = res.id;
                        this.browserStorageService.addToLocalStorage(StorageKey.USER, user);
                        this.router.navigateByUrl('');
                    } else {
                        throw new Error('[EditProfileComponent] [createMyProfile$] user is undefined')
                    }
                })
            );
        }
    }

    // custom validators
    private genderValidate(
        ...args: string[]
    ): (control: AbstractControl) => ValidationErrors | null {
        return (control: AbstractControl): ValidationErrors | null => {
            const isIncludes = args.includes(control.value);
            return isIncludes ? null : { isIncludes };
        };
    }

    private setValidatators(): void {
        this.firstName.setValidators([
            Validators.required,
            Validators.minLength(4),
            Validators.maxLength(20)
        ]);
        this.lastName.setValidators([
            Validators.required,
            Validators.minLength(4),
            Validators.maxLength(20)
        ]);
        this.gender.setValidators([
            Validators.required,
            this.genderValidate('male', 'female')
        ]);
        this.language.setValidators([
            Validators.required
        ]);
        this.birthDate.setValidators([
            Validators.required,
            // Validators.pattern(IsoDatePattern)
        ]);
        this.city.setValidators([
            Validators.required
        ]);
        this.phoneNumber.setValidators([
            Validators.required,
            Validators.pattern(ValidPhoneNumber)
        ]);
        this.description.setValidators([
            Validators.required,
            Validators.minLength(4),
            Validators.maxLength(300)
        ]);
    }

    // controls:
    get firstName(): UntypedFormControl {
        return this.formGroup.controls.firstName as UntypedFormControl;
    }
    get lastName(): UntypedFormControl {
        return this.formGroup.controls.lastName as UntypedFormControl;
    }
    get gender(): UntypedFormControl {
        return this.formGroup.controls.gender as UntypedFormControl;
    }
    get language(): UntypedFormControl {
        return this.formGroup.controls.language as UntypedFormControl;
    }
    get birthDate(): UntypedFormControl {
        return this.formGroup.controls.birthDate as UntypedFormControl;
    }
    get city(): UntypedFormControl {
        return this.formGroup.controls.city as UntypedFormControl;
    }
    get phoneNumber(): UntypedFormControl {
        return this.formGroup.controls.phoneNumber as UntypedFormControl;
    }
    get description(): UntypedFormControl {
        return this.formGroup.controls.description as UntypedFormControl;
    }
}
