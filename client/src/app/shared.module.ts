import { NgModule } from '@angular/core';
import { FormModule } from './form/form.module';

const SharedModules = [
    FormModule
];

@NgModule({
    imports: [...SharedModules],
    exports: [...SharedModules],
})
export class SharedModule {}
