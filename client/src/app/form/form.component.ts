import { Component, EventEmitter, Input, Output } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss'],
})
export class FormComponent {
    @Input() formGroup: UntypedFormGroup = new UntypedFormGroup({});
    @Input() title: string | undefined;
    @Input() buttonText: string | undefined;

    @Output() formSubmit: EventEmitter<unknown> = new EventEmitter();

    get errorMessage(): string | undefined {
        if (this.formGroup.errors === null) {
            return undefined;
        }
        return this.formGroup.errors.formError;
    }

    public submit(): void {
        if (this.formGroup === undefined) {
            throw Error('Error: The formGroup is undefined');
        }
        if (this.formGroup.invalid) {
            this.formGroup.setErrors({
              formError: 'From is invalid. Please, recheck all the fields'
            });
            return;
        }
        this.formSubmit.emit();
    }
}
