import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormErrorComponent } from './components/form-error/form-error.component';
import { InputFieldComponent } from './controls/input-field/input-field.component';
import { SelectFieldComponent } from './controls/select-field/select-field.component';
import { FormComponent } from './form.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [
        FormComponent,
        InputFieldComponent,
        SelectFieldComponent,
        ReactiveFormsModule
    ],
    declarations: [
        FormComponent,
        FormErrorComponent,
        InputFieldComponent,
        SelectFieldComponent,
    ]
})
export class FormModule {}
