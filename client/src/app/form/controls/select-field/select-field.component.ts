import { Component, Input } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';

@Component({
    selector: 'app-select-field',
    templateUrl: 'select-field.component.html',
    styleUrls: ['select-field.component.scss'],
})
export class SelectFieldComponent {
    @Input() set control(value: UntypedFormControl) {
        this._control = value;
    }

    get control(): UntypedFormControl {
        if (this._control === undefined) {
            throw Error('[InputFieldComponent] control is undefined');
        }
        return this._control;
    }

    @Input() values: string[] | undefined;
    @Input() label: string | undefined;
    @Input() name: string | undefined;
    @Input() placeholder: string | undefined;

    private _control: UntypedFormControl | undefined;

    constructor() {}
}
