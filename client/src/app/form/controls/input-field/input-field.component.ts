import { Component, Input } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';

@Component({
    selector: 'app-input-field',
    templateUrl: 'input-field.component.html',
    styleUrls: ['input-field.component.scss'],
})
export class InputFieldComponent {
    @Input() set control(value: UntypedFormControl) {
        this._control = value;
    }

    get control(): UntypedFormControl {
        if (this._control === undefined) {
            throw Error('[InputFieldComponent] control is undefined');
        }
        return this._control;
    }

    @Input() type: string | undefined;
    @Input() label: string | undefined;
    @Input() name: string | undefined;
    @Input() placeholder: string | undefined;

    private _control: UntypedFormControl | undefined;

    constructor() {}
}
