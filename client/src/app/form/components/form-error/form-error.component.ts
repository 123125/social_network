import { Component, Input } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';

@Component({
    selector: 'app-form-error',
    templateUrl: 'form-error.component.html',
    styleUrls: ['form-error.component.scss']
})
export class FormErrorComponent {
    @Input() control: UntypedFormControl | undefined;

    // Note: it is used if it's necessary to pass message directly instead of checking control
    @Input() message: string | undefined;

    get errorMessage(): string | undefined {
        if (this.message !== undefined) {
            return this.message;
        }
        if (
            this.control === undefined ||
            this.control.errors === null ||
            (this.control.pristine &&
            !this.control.touched &&
            !this.control.dirty)
        ) {
            return undefined;
        }

        const errors = this.control.errors;
        switch (true) {
            case errors.required:
                return 'The field is required';

            case errors.maxlength !== undefined:
                return `Max length should be less than ${errors.maxlength.requiredLength} symbols`;

            case errors.minlength !== undefined:
                return `Min length should be more than ${errors.minlength.requiredLength} symbols`;

            case errors.pattern !== undefined:
                return 'A value of the field is incorrect';

            default:
                return Object.values(errors)[0] as string;
        }
    }
}
