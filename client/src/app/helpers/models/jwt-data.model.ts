export type JwtData = {
    id: number,
    email: string,
    login: string,
    profileId: number,
    iat: number,
}