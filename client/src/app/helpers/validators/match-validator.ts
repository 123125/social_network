import { UntypedFormControl, ValidatorFn } from "@angular/forms";

export const MatchValidator = (
    control: UntypedFormControl,
    matchingControl: UntypedFormControl,
    errorMessage: string
): ValidatorFn => {
    return (): {matchError: string} | null => {
        if (control.value !== matchingControl.value)
          return { matchError: errorMessage };
        return null;
    };
}