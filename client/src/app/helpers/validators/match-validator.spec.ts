import { UntypedFormControl } from "@angular/forms";
import { MatchValidator } from "./match-validator";

describe('MatchValidator', () => {
    const someErrorMessage = 'Some error message';
    let firstControl: UntypedFormControl;
    let secondControl: UntypedFormControl;

    beforeEach(() => {
        firstControl = new UntypedFormControl(null);
        secondControl = new UntypedFormControl(null);
    });

    it('should return null if controls values do mutch', () => {
        firstControl.setValue('78ui&*UI');
        secondControl.setValue('78ui&*UI');
        const validator = MatchValidator(firstControl, secondControl, someErrorMessage);
        expect(validator(secondControl)).toBeNull();
    });

    it('should return null if controls values do not mutch', () => {
        firstControl.setValue('78ui&*UI');
        secondControl.setValue('78ui&*UI123');
        const validator = MatchValidator(firstControl, secondControl, someErrorMessage);
        expect(validator(secondControl)?.matchError).toEqual(someErrorMessage);
    });
});