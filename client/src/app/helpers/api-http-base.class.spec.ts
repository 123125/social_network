import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { ApiHttpEntity } from './api-http-base.class';

describe('ApiHttpEntity', () => {
    const endpointsModel = {
        get: '',
        put: '',
        post: '',
        delete: '',
    }
    let apiHttpEntity: ApiHttpEntity<{}, {}>;
    let httpClient: HttpClient;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports:[HttpClientTestingModule],
        });
        httpClient = TestBed.get(HttpClient);
        apiHttpEntity = new ApiHttpEntity(endpointsModel, httpClient);

        spyOn(apiHttpEntity.loading$, 'next');
        spyOn(apiHttpEntity.entity$, 'next');
    });

    describe('get$', () => {
        it('should call loading$.next with true argument', () => {
            apiHttpEntity.get$({});
            expect(apiHttpEntity.loading$.next).toHaveBeenCalledWith(true);
        });

        it('should call loading$.next with true argument', () => {
            apiHttpEntity.get$({}).pipe().subscribe(() => {
                expect(apiHttpEntity.loading$.next).toHaveBeenCalledWith(false);
            }).unsubscribe();
        });

        it('should call entity$.next', () => {
            apiHttpEntity.get$({}).subscribe(() => {
                expect(apiHttpEntity.entity$.next).toHaveBeenCalled();
            }).unsubscribe();
        });

        it('should call httpClient.get', () => {
            spyOn(httpClient, 'get').and.returnValue(of());
            apiHttpEntity.get$({});
            expect(httpClient.get).toHaveBeenCalled();
        });
    });

    describe('post$', () => {
        it('should call loading$.next with true argument', () => {
            apiHttpEntity.post$({});
            expect(apiHttpEntity.loading$.next).toHaveBeenCalledWith(true);
        });

        it('should call loading$.next with true argument', () => {
            apiHttpEntity.post$({}).subscribe(() => {
                expect(apiHttpEntity.loading$.next).toHaveBeenCalledWith(false);
            }).unsubscribe();
        });

        it('should call entity$.next', () => {
            apiHttpEntity.post$({}).subscribe(() => {
                expect(apiHttpEntity.entity$.next).toHaveBeenCalled();
            }).unsubscribe();
        });

        it('should call httpClient.post', () => {
            spyOn(httpClient, 'post').and.returnValue(of());
            apiHttpEntity.post$({});
            expect(httpClient.post).toHaveBeenCalled();
        });
    });

    describe('put$', () => {
        const id = 123;
        it('should call loading$.next with true argument', () => {
            apiHttpEntity.put$(id, {});
            expect(apiHttpEntity.loading$.next).toHaveBeenCalledWith(true);
        });

        it('should call loading$.next with true argument', () => {
            apiHttpEntity.put$(id, {}).subscribe(() => {
                expect(apiHttpEntity.loading$.next).toHaveBeenCalledWith(false);
            }).unsubscribe();
        });

        it('should call entity$.next', () => {
            apiHttpEntity.put$(id, {}).subscribe(() => {
                expect(apiHttpEntity.entity$.next).toHaveBeenCalled();
            }).unsubscribe();
        });

        it('should call httpClient.put', () => {
            spyOn(httpClient, 'put').and.returnValue(of());
            apiHttpEntity.put$(id, {});
            expect(httpClient.put).toHaveBeenCalled();
        });
    });

    describe('delete$', () => {
        const id = 123;
        it('should call loading$.next with true argument', () => {
            apiHttpEntity.delete$(id);
            expect(apiHttpEntity.loading$.next).toHaveBeenCalledWith(true);
        });

        it('should call loading$.next with true argument', () => {
            apiHttpEntity.delete$(id).subscribe(() => {
                expect(apiHttpEntity.loading$.next).toHaveBeenCalledWith(false);
            }).unsubscribe();
        });

        it('should call entity$.next', () => {
            apiHttpEntity.delete$(id).subscribe(() => {
                expect(apiHttpEntity.entity$.next).toHaveBeenCalled();
            }).unsubscribe();
        });

        it('should call httpClient.put', () => {
            spyOn(httpClient, 'delete').and.returnValue(of());
            apiHttpEntity.delete$(id);
            expect(httpClient.delete).toHaveBeenCalled();
        });
    });

});