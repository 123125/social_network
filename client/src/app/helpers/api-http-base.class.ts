import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { UntypedFormGroup } from '@angular/forms';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

const httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
type HttpOptions = {
    headers: HttpHeaders
}

const options = {
    headers: httpHeaders
};

type EndpointsModel = {
    get?: string;
    put?: string;
    post?: string;
    delete?: string;
}

export class ApiHttpEntity<FormModel, ResponseModel = FormModel> {
    protected readonly baseUrl = environment.baseUrl;
    protected options: HttpOptions = options; 

    public entity$: BehaviorSubject<
        ResponseModel | null
    > = new BehaviorSubject<ResponseModel | null>(null);
    public loading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor(
        private readonly endpoints: EndpointsModel,
        private readonly http: HttpClient,
    ) {}

    public get$(params?: Record<string, string | number | undefined>): Observable<ResponseModel & {id: string}> {
        const url = this.getUrl(this.endpoints.get, 'get$', params);
        this.loading$.next(true);
        return this.http.get<ResponseModel & {id: string}>(url, this.options).pipe(
            tap((res) => {
                this.entity$.next(res);
                this.loading$.next(false);
            }),
            catchError((ex) => this.errorHandler(ex))
        );
    }

    public post$(
        data: FormModel,
        formGroup?: UntypedFormGroup
    ): Observable<ResponseModel> {
        const url = this.getUrl(this.endpoints.post, 'post$');
        this.loading$.next(true);
        return this.http.post<ResponseModel>(url, data, this.options).pipe(
            tap((res) => {
                this.entity$.next(res);
                this.loading$.next(false);
            }),
            catchError((ex) => this.errorHandler(ex, formGroup))
        );
    }

    public put$(
        id: number,
        data: FormModel,
        formGroup?: UntypedFormGroup
    ): Observable<ResponseModel> {
        const url = this.getUrl(this.endpoints.put, 'put$', { id });
        this.loading$.next(true);
        return this.http.put<ResponseModel>(url, data, this.options).pipe(
            tap((res) => {
                this.entity$.next(res);
                this.loading$.next(false);
            }),
            catchError((ex) => this.errorHandler(ex, formGroup))
        );
    }

    public delete$(id: number): Observable<void> {
        const url = this.getUrl(this.endpoints.delete, 'delete$', { id });
        this.loading$.next(true);
        return this.http.delete<void>(url, this.options).pipe(
            tap(() => {
                this.loading$.next(false);
            }),
            catchError((ex) => this.errorHandler(ex))
        );
    }

    public setOptions(options: HttpOptions): void {
        this.options = options;
    }

    private errorHandler(
        ex: HttpErrorResponse,
        formGroup?: UntypedFormGroup
    ): never {
        this.loading$.next(false);
        if (
            ex !== null &&
            ex !== undefined &&
            formGroup !== undefined &&
            ex.error.error !== undefined
        ) {
            formGroup.setErrors({ formError: ex.error.error });
        }
        throw ex;
    }

    private getUrl(
        endpoint: string | undefined,
        method: string,
        params: Record<string, string | number | undefined> = {}
    ): string {
        if (endpoint === undefined) {
            throw Error(`[ApiHttpEntity] [${method}] Endpoint for request is undefined`);
        }

        let fullUrl = this.baseUrl + '/' + endpoint;
        for (const paramKey in params) {
            if (params[paramKey]) {
                const paramValue = params[paramKey];
                if (paramValue !== undefined) {
                    fullUrl = fullUrl.replace(`{${paramKey}}`, paramValue.toString());
                }
            }
          }

        return fullUrl;
    }

}
