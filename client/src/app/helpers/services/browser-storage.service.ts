import { Injectable } from '@angular/core';

export enum StorageKey {
    TOKEN = 'access_token',
    USER = 'user'
}

@Injectable({
    providedIn: 'root'
})
export class BrowserStorageService {
    constructor() {}

    public addToLocalStorage(key: StorageKey, data: unknown): void {
        localStorage.setItem(key, JSON.stringify(data));
    }

    public removeFromLocalStorage(key: StorageKey): void {
        localStorage.removeItem(key);
    }

    public getFromStorage<T>(key: StorageKey): T | undefined {
        const data = localStorage.getItem(key) || undefined;
        if (data === undefined) { return data; }
        return JSON.parse(data);
    }
}
