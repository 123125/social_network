import { TestBed } from '@angular/core/testing';
import { BrowserStorageService, StorageKey } from './browser-storage.service';

describe('BrowserStorage', () => {
    let browserStorage: BrowserStorageService;
    const tokenString = 'token';

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [BrowserStorageService],
        });
        browserStorage = TestBed.inject(BrowserStorageService);
    });

    it('addToLocalStorage should call localStorage.setItem', () => {
        spyOn(window.localStorage, 'setItem');
        browserStorage.addToLocalStorage(StorageKey.TOKEN, tokenString);
        const jsonToken = JSON.stringify(tokenString)
        expect(localStorage.setItem).toHaveBeenCalledOnceWith(StorageKey.TOKEN, jsonToken);
    });

    it('removeFromLocalStorage should call localStorage.removeItem', () => {
        spyOn(window.localStorage, 'removeItem');
        browserStorage.removeFromLocalStorage(StorageKey.TOKEN);
        expect(localStorage.removeItem).toHaveBeenCalledOnceWith(StorageKey.TOKEN);
    });

    describe('getFromStorage', () => {
        it('should call localStorage.getItem', () => {
            spyOn(window.localStorage, 'getItem');
            browserStorage.getFromStorage(StorageKey.TOKEN);
            expect(localStorage.getItem).toHaveBeenCalledOnceWith(StorageKey.TOKEN);
        });
    
        it('should return undefined if localStorage does not have value', () => {
            spyOn(window.localStorage, 'getItem').and.returnValue(null);
            const result = browserStorage.getFromStorage(StorageKey.TOKEN);
            expect(result).toBe(undefined);
        });
    
        it('should return value if localStorage does have value', () => {
            spyOn(window.localStorage, 'getItem').and.returnValue(JSON.stringify(tokenString));
            const result = browserStorage.getFromStorage(StorageKey.TOKEN);
            expect(result).toBe(tokenString);
        });
    });
    
});