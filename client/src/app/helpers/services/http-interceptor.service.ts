import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from '../../auth/auth.service';
import { BrowserStorageService, StorageKey } from './browser-storage.service';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
    constructor(
        private readonly browserStorageService: BrowserStorageService,
        private readonly authService: AuthService
    ) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = this.browserStorageService.getFromStorage(StorageKey.TOKEN) || undefined;
        const request = token !== undefined
            ? req.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`
                }
            })
            : req;

        return next.handle(request).pipe(
            tap(
                (event) => {},
                (err) => {
                    const isHttpError = err instanceof HttpErrorResponse;
                    if (!isHttpError) {
                        return;
                    }

                    // TODO: move status code in the separate file
                    if (err.status === 401) {
                        console.warn('Unauthorized');
                        this.authService.logout();
                    }
                }
            )
        );
    }
}
