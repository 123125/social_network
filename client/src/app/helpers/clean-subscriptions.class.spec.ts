import { TestBed } from '@angular/core/testing';
import { Subscription } from 'rxjs';
import { CleanSubscriptions } from './clean-subscriptions.class';

describe('CleanSubscriptions', () => {
    let cleanSubscribtions: CleanSubscriptions;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [CleanSubscriptions],
        });
        cleanSubscribtions = TestBed.inject(CleanSubscriptions);
    });

    it('addSubscription should add subscription in subscribtions array', () => {
        cleanSubscribtions.addSubscription(new Subscription());
        expect(cleanSubscribtions['subscribtions'].length).toEqual(1);
    });

    it('cleanSubscribtions should remove all the subscribtions from array', () => {
        cleanSubscribtions.cleanSubscription();
        expect(cleanSubscribtions['subscribtions'].length).toEqual(0);
    });

    it('ngOnDestroy should call cleanSubscribtions', () => {
        spyOn(cleanSubscribtions, 'cleanSubscription');
        cleanSubscribtions.ngOnDestroy();
        expect(cleanSubscribtions.cleanSubscription).toHaveBeenCalled();
    });
})