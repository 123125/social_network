import {  Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Injectable()
export class CleanSubscriptions implements OnDestroy {
    private subscribtions: Subscription[] = [];

    constructor() {}

    ngOnDestroy(): void {
        this.cleanSubscription();
    }

    addSubscription(subscription: Subscription): void {
        this.subscribtions.push(subscription);
    }

    cleanSubscription(): void {
        this.subscribtions.forEach((subscribtion) => subscribtion.unsubscribe());
        this.subscribtions = [];
    }
}
