export interface ILogger {
    info: (...arg: unknown[]) => void;
    warn: (...arg: unknown[]) => void;
    error: (...arg: unknown[]) => void;
}