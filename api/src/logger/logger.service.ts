import { Logger } from 'tslog';
import { ILogger } from './logger.interface';

export class LoggerService implements ILogger {
    private _logger: Logger;

    constructor() {
        this._logger = new Logger({
            displayInstanceName: false,
			displayLoggerName: false,
			displayFilePath: 'hidden',
			displayFunctionName: false,
        });
    }

    public error(...arg: unknown[]) {
        this._logger.error(...arg);
    }

    public warn(...arg: unknown[]) {
        this._logger.warn(...arg);
    }

    public info(...arg: unknown[]) {
        this._logger.info(...arg);
    }
}