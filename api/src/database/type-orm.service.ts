import { DataSource, DataSourceOptions } from 'typeorm'
import { IConfigService } from '../config/config.service.interface';
import { ILogger } from '../logger/logger.interface';
import { FriendRelation } from '../friend-relation/friend-relation.entity';
import { Profile } from '../profiles/profile.entity';
import { User } from '../users/user.entity';
export class TypeOrmService {
    private _appDataSource: DataSource | undefined;

    get appDataSource(): DataSource {
        if (this._appDataSource === undefined) {
            throw new Error(`[TypeOrmService] appDataSource is not initialized`);
        }
        return this._appDataSource;
    }

    get dataSourceOptions(): DataSourceOptions {
        return {
            type: this.configService.get('DB_TYPE') as 'postgres',
            database: this.configService.get('DB_NAME'),
            username: this.configService.get('DB_USERNAME'),
            password: this.configService.get('DB_PASSWORD'),
            host: this.configService.get('DB_HOST'),
            port: Number(this.configService.get('DB_PORT')),
            synchronize: this.configService.get('DB_SYNCHRONIZE') === 'true',
            entities: [User, Profile, FriendRelation]
        }
    }

    constructor(
        private readonly configService: IConfigService,
        private readonly logger: ILogger
    ) {
        this._appDataSource = new DataSource(this.dataSourceOptions);
    }

    async connect(): Promise<void> {
        await this.appDataSource.initialize()
            .then(() => {
                this.logger.info(`[TypeOrmService] [connect] database is connected`);
            }, (error: unknown) => {
                this.logger.error(`[TypeOrmService] [connect] ${error}`);
            });
    }

    async disconnect(): Promise<void> {
        await this.appDataSource.destroy();
    }
}