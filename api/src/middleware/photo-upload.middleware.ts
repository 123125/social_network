import { Request, Response, NextFunction } from 'express';
import multer from 'multer';
import { IMiddleware } from './middleware.interface';

export class PhotoUploadMiddleware implements IMiddleware {
    execute(_req: Request, _res: Response, next: NextFunction): void {
        const storage = multer.diskStorage({
                destination: function (req, _file, cb) {
                  console.log(req.body.photo) // YAY, IT'S POPULATED
                  console.log(req.file, req.files) // YAY, IT'S POPULATED
                  cb(null, 'uploads/')
                },                    
                filename: function (_req, file, cb) {
                  cb(null, file.fieldname + '-' + Date.now())
                }                     
              });
        multer({storage}).single("photo");
        next();
    }
}