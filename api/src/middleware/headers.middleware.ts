import { Request, Response, NextFunction } from 'express';
import { IMiddleware } from './middleware.interface';

export class HeadersMiddleware implements IMiddleware {
    execute (_req: Request, res: Response, next: NextFunction): void {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Headers', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization");
        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', 'true');

        next();
    }
}