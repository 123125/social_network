import { Request, Response, NextFunction } from 'express';
import { verify } from 'jsonwebtoken';
import { RoutesPath } from '../app';
import { ConfigService } from '../config/config.service';
import { HttpErrorMessage } from '../errors/http-error-message.enum';
import { HttpError } from '../errors/http-error.class';
import { IMiddleware } from './middleware.interface';

export type JwtPayload = {
    id: string,
    email: string,
    login: string,
    profileId: string,
};

export type RequestWithPayload = Request & { user: JwtPayload };

export class AuthMiddleware implements IMiddleware {
    constructor(
        private configService: ConfigService
    ) {}

    execute(req: Request, _res: Response, next: NextFunction): void {
        console.log(req.originalUrl)
        if (req.originalUrl.includes(RoutesPath.USERS)) {
            return next();
        }
        if (req.headers.authorization !== undefined) {
            const token = req.headers.authorization.split(' ')[1];
            if (token === undefined) {
                return next(new HttpError(401, HttpErrorMessage.NOT_AUTHORIZED));
            }

            verify(token, this.configService.get('JWT_SECRET'), (err: unknown, payload: unknown) => {
                if (err !== null) {
                    return next(new HttpError(401, HttpErrorMessage.NOT_AUTHORIZED));
                } else {
                    (req as RequestWithPayload).user = (payload as JwtPayload);
                    return next();
                }
            });
        } else {
            return next(new HttpError(401, HttpErrorMessage.NOT_AUTHORIZED));
        }
    }
}