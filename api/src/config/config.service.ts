import { config, DotenvParseOutput } from 'dotenv';
import { ILogger } from '../logger/logger.interface';
import { IConfigService } from './config.service.interface';

export class ConfigService implements IConfigService {
    private _config: DotenvParseOutput | undefined;

    get config(): DotenvParseOutput {
        if (this._config === undefined) {
            throw new Error('Config was not set');
        }
        return this._config;
    }

    constructor(private readonly logger: ILogger) {
        const result = config();
        if (result.error) {
            this.logger.error('There is no env file');
        } else {
            this._config = result.parsed;
        }
    }

    get(key: string): string {
        const value = this.config[key];
        if (value === undefined) {
            throw new Error(`There is no ${key} in config file`);
        }
        return value;
    }
}