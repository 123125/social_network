import { Request, Response, NextFunction } from 'express';
import { HttpErrorMessage } from '../errors/http-error-message.enum';
import { HttpError } from '../errors/http-error.class';
import { ILogger } from '../logger/logger.interface';
import { RequestWithPayload } from '../middleware/auth.middleware';
import { UsersService } from '../users/users.service';
import { ProfilesService } from './profiles.service';
import { RoutesPath } from '../app';
import * as fs from 'fs';
import * as path from 'path';

export class ProfilesController {
    constructor(
        private readonly profilesService: ProfilesService,
        private readonly usersService: UsersService,
        private readonly logger: ILogger,
    ) {}

    public async createMyProfile(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        try {
            const userId = (req as RequestWithPayload).user.id;
            const profileId = (req as RequestWithPayload).user.profileId;
            const existedProfile = await this.profilesService.getProfileById(profileId);
            if (existedProfile !== undefined) {
                return next(new HttpError(400, HttpErrorMessage.PROFILE_ALREADY_EXIST));
            }

            const profile = await this.profilesService.createProfile({
                ...req.body,
                userId
            });
            await this.usersService.addProfileIdToUser(userId, profile.id);

            if (profile === undefined) {
                return next(new HttpError(404, HttpErrorMessage.NOT_FOUND));
            }

            res.status(201).json(profile);
        } catch (error) {
            this.logger.error(`[ProfilesController] [createMyProfile] ${error}`);
            return next(error);
        }
    }

    public async getMyProfile(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        try {
            const userId = (req as RequestWithPayload).user.id;

            const profile = await this.profilesService.getProfileByUserId(userId);
            if (profile === undefined) {
                return next(new HttpError(404, HttpErrorMessage.NOT_FOUND));
            }

            res.status(200).json(profile);
        } catch (error) {
            this.logger.error(`[ProfilesController] [getMyProfile] ${error}`);
            return next(error);
        }
    }

    public async addAvatarForMyProfile(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        try {
            const photo = req.file;
            if (photo === undefined) {
                return next(new HttpError(400, HttpErrorMessage.BAD_REQUEST));
            }
            const userId = (req as RequestWithPayload).user.id;

            const profile = await this.profilesService.getProfileByUserId(userId);
            if (profile === undefined) {
                return next(new HttpError(404, HttpErrorMessage.NOT_FOUND));
            }
            
            const avatarUrl = photo.path.replace(/\//g, '/').replace(RoutesPath.UPLOADS, '');

            const oldAvatarUrl = path.dirname('') + '/' + RoutesPath.UPLOADS + profile.avatarUrl
            if (fs.existsSync(oldAvatarUrl)) {
                fs.unlinkSync(oldAvatarUrl);
            }

            await this.profilesService.updateProfile(profile, { avatarUrl });
            
            res.status(200).send({avatarUrl});
        } catch (error) {
            this.logger.error(`[ProfilesController] [addAvatarForMyProfile] ${error}`);
            return next(error);
        }
    }

    public async getOne(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        try {
            const profileId = req.params['id'];
            if (profileId === undefined) {
                return next(new HttpError(400, HttpErrorMessage.BAD_REQUEST));
            }

            const profile = await this.profilesService.getProfileById(profileId);
            if (profile === undefined) {
                return next(new HttpError(404, HttpErrorMessage.NOT_FOUND));
            }

            res.status(200).json(profile);
        } catch (error) {
            this.logger.error(`[ProfilesController] [getOne] ${error}`);
            return next(error);
        }
    }

    public async getList(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        try {
            const { skip, take, search } = req.query;
            const currentUser = (req as RequestWithPayload).user;
            if (skip === undefined || take === undefined) {
                return next(new HttpError(400, HttpErrorMessage.BAD_REQUEST));
            }

            const profiles = await this.profilesService.getProfileList({
                skip: Number(skip),
                take: Number(take),
                search: search?.toString() ?? undefined
            }, currentUser);

            res.status(200).json({
                list: profiles,
                page: Number(skip) + 1,
            });
        } catch (error) {
            this.logger.error(`[ProfilesController] [getList] ${error}`);
            return next(error);
        }
    }

    public async updateMyProfile(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        try {
            const userId = (req as RequestWithPayload).user.id;

            const profile = await this.profilesService.getProfileByUserId(userId);
            if (profile === undefined) {
                return next(new HttpError(404, HttpErrorMessage.NOT_FOUND));
            }

            await this.profilesService.updateProfile(profile, req.body);
            res.status(200).json(profile);
        } catch (error) {
            this.logger.error(`[ProfilesController] [updateProfile] ${error}`);
            return next(error);
        }
    }

    public async deleteById(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        try {
            const profileId = req.params['id'];
            if (profileId === undefined) {
                return next(new HttpError(400, HttpErrorMessage.BAD_REQUEST));
            }

            const result = await this.profilesService.deleteProfile(profileId);
            if (result.affected === 0) {
                return next(new HttpError(404, HttpErrorMessage.NOT_FOUND));
            }
            res.status(200).send();
        } catch (error) {
            this.logger.error(`[ProfilesController] [deleteById] ${error}`);
            return next(error);
        }
    }
}