import { IsDateString, IsEnum, IsNotEmpty, IsNumber, IsOptional, IsString, Length } from 'class-validator';
import { ProfileGender } from '../models/profile.enum';

export class CreateProfileDto {
    @IsNumber()
    @IsNotEmpty()
    userId!: number;
    
    @IsString()
    @IsNotEmpty()
    @Length(4, 20)
    firstName!: string;

    @IsString()
    @IsNotEmpty()
    @Length(4, 20)
    lastName!: string;

    @IsString()
    @IsNotEmpty()
    phoneNumber!: string;

    @IsEnum(ProfileGender)
    @IsNotEmpty()
    gender!: ProfileGender;

    @IsString()
    @IsNotEmpty()
    language!: string;

    @IsDateString()
    @IsNotEmpty()
    birthDate!: string;

    @IsString()
    @IsNotEmpty()
    city!: string;

    @IsOptional()
    @IsString()
    statusText!: string;

    @IsOptional()
    @IsString()
    description!: string;

    @IsOptional()
    @IsString()
    avatarUrl!: string;
}