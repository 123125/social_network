import { IsNotEmpty,  } from 'class-validator';
import { IsFile } from '../../common/decorators/file-validation';

export class AddAvatarDto {
    @IsFile({ mime: ['image/jpg', 'image/png']})
    @IsNotEmpty()
    photo!: BinaryData;
}