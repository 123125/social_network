import { IsNumber, IsOptional, IsString } from 'class-validator';

export class GetProfileList {
    @IsOptional()
    @IsNumber()
    take!: number;

    @IsOptional()
    @IsNumber()
    skip!: number;

    @IsOptional()
    @IsString()
    search!: string | undefined;
}