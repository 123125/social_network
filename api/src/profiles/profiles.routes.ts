import { BaseRoute } from '../common/base.route';
import { ConfigService } from '../config/config.service';
import { TypeOrmService } from '../database/type-orm.service';
import { FriendRelationService } from '../friend-relation/friend-relation.service';
import { ILogger } from '../logger/logger.interface';
import { PhotoUploadMiddleware } from '../middleware/photo-upload.middleware';
import { UsersService } from '../users/users.service';
import { ProfilesController } from './profiles.controller';
import { ProfilesService } from './profiles.service';

enum ProfilesRoutesPath {
    GET_ONE = '/:id',
    MY_PROFILE = '/my',
    GET_LIST = '/list',
    DELETE = '/:id',
    ADD_AVATAR = '/my/photo',
}
export class ProfilesRoutes extends BaseRoute {
    constructor(
        private readonly databaseService: TypeOrmService,
        private readonly configService: ConfigService,
        logger: ILogger,
    ) {
        super(logger);
        const friendRelationService = new FriendRelationService(this.databaseService);
        const profilesService = new ProfilesService(this.databaseService, friendRelationService);
        const usersService = new UsersService(this.databaseService, this.configService);
        const controller = new ProfilesController(
            profilesService,
            usersService,
            this.logger
        );
        const photoUploadMiddleware = new PhotoUploadMiddleware();
        this.routes = [
            {
                path: ProfilesRoutesPath.MY_PROFILE,
                method: 'post',
                handler: controller.createMyProfile.bind(controller),
            },
            {
                path: ProfilesRoutesPath.MY_PROFILE,
                method: 'get',
                handler: controller.getMyProfile.bind(controller),
            },
            {
                path: ProfilesRoutesPath.GET_LIST,
                method: 'get',
                handler: controller.getList.bind(controller),
            },
            {
                path: ProfilesRoutesPath.GET_ONE,
                method: 'get',
                handler: controller.getOne.bind(controller),
            },
            {
                path: ProfilesRoutesPath.MY_PROFILE,
                method: 'put',
                handler: controller.updateMyProfile.bind(controller),
            },
            {
                path: ProfilesRoutesPath.DELETE,
                method: 'delete',
                handler: controller.deleteById.bind(controller),
            },
            {
                path: ProfilesRoutesPath.ADD_AVATAR,
                method: 'post',
                handler: controller.addAvatarForMyProfile.bind(controller),
                middleware: photoUploadMiddleware   
            },
        ];
        this.bindRoutes(this.routes);
    }
}