import { In, Like, Not, Repository, UpdateResult } from 'typeorm';
import { TypeOrmService } from '../database/type-orm.service';
import { FriendRelationService } from '../friend-relation/friend-relation.service';
import { CreateProfileDto } from './dto/create-profile.dto';
import { GetProfileList } from './dto/get-profile-list.dto';
import { Profile } from './profile.entity';
import { JwtPayload } from '../middleware/auth.middleware';

export class ProfilesService {
    private profileRepository: Repository<Profile>;

    constructor(
        private readonly databaseService: TypeOrmService,
        private readonly friendRelationService: FriendRelationService
    ) {
        this.profileRepository = this.databaseService.appDataSource.getRepository(Profile);
    }

    public async createProfile(
        createProfileDto: CreateProfileDto
    ): Promise<Profile> {
        const newProfile = new Profile();
        newProfile.userId = createProfileDto.userId;
        newProfile.firstName = createProfileDto.firstName;
        newProfile.lastName = createProfileDto.lastName;
        newProfile.phoneNumber = createProfileDto.phoneNumber;
        newProfile.gender = createProfileDto.gender;
        newProfile.language = createProfileDto.language;
        newProfile.birthDate = createProfileDto.birthDate;
        newProfile.city = createProfileDto.city;
        newProfile.statusText = createProfileDto.statusText;
        newProfile.description = createProfileDto.description;

        return this.profileRepository.save(newProfile);
    }

    public async getProfileById(id: string): Promise<Profile | undefined> {
        const profile = await this.profileRepository.manager.findOneById<Profile>(Profile, id);
        return profile !== null ? profile : undefined;
    }

    public async getProfileByUserId(userId: string): Promise<Profile | undefined> {
        const profile = await this.profileRepository.manager.findOneBy<Profile>(Profile, { userId: Number(userId) });
        return profile !== null ? profile : undefined;
    }

    public async getProfileList(queryParams: GetProfileList, currentUser: JwtPayload): Promise<Profile[]> {
        // get all the friends ids of the current user 
        const currentUserFriendsIds = await this.friendRelationService.getAllFriends(
            currentUser.profileId
        ).then((friends) => friends.map((friend) => friend.friendId));

        const { skip, take, search } = queryParams;
        
        // general where conditions for finding
        const commonWhere = { 
            userId : Not(Number(currentUser.id)),
            id: Not(In(currentUserFriendsIds)),
        };

        // if search is exsted firstName and lastName are checking otherwise only commonWhere is used
        const whereOption = search !== undefined 
            ? { where: [
                {
                    ...commonWhere,
                    firstName: Like(`%${search}%`)
                },
                {
                    ...commonWhere,
                    lastName: Like(`%${search}%`)
                },
            ] }
            : { where: commonWhere };

        return this.profileRepository.manager.find<Profile>(Profile, {
            skip,
            take,
            ...whereOption
        });
    }

    public async updateProfile(
        profile: Profile,
        updateProfileDto: Partial<CreateProfileDto>
    ): Promise<Profile> {
        if (updateProfileDto.firstName !== undefined) {
            profile.firstName = updateProfileDto.firstName;
        }
        if (updateProfileDto.lastName !== undefined) {
            profile.lastName = updateProfileDto.lastName;
        }
        if (updateProfileDto.phoneNumber !== undefined) {
            profile.phoneNumber = updateProfileDto.phoneNumber;
        }
        if (updateProfileDto.gender !== undefined) {
            profile.gender = updateProfileDto.gender;
        }
        if (updateProfileDto.language !== undefined) {
            profile.language = updateProfileDto.language;
        }
        if (updateProfileDto.birthDate !== undefined) {
            profile.birthDate = updateProfileDto.birthDate;
        }
        if (updateProfileDto.city !== undefined) {
            profile.city = updateProfileDto.city;
        }
        if (updateProfileDto.statusText !== undefined) {
            profile.statusText = updateProfileDto.statusText;
        }
        if (updateProfileDto.description !== undefined) {
            profile.description = updateProfileDto.description;
        }
        if (updateProfileDto.avatarUrl !== undefined) {
            profile.avatarUrl = updateProfileDto.avatarUrl;
        }
        return this.profileRepository.manager.save(profile);
    }

    public async deleteProfile(
        profileId: string
    ): Promise<UpdateResult> {
        return this.profileRepository.manager.softDelete(Profile, { id: profileId });
    }
}