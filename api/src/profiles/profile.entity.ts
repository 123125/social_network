import { Column, CreateDateColumn, DeleteDateColumn, Entity, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { User } from '../users/user.entity';
import { ProfileGender, ProfileStatus } from './models/profile.enum';

@Entity()
export class Profile {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    firstName!: string;

    @Column()
    lastName!: string;

    @Column()
    phoneNumber!: string;

    @Column({
        type: 'enum',
        enum: ProfileGender,
        default: null
    })
    gender!: ProfileGender;

    @Column()
    language!: string;

    @Column({
        type: 'date'
    })
    birthDate!: string;

    @Column()
    city!: string;

    @Column({
        type: 'text',
        nullable: true
    })
    statusText: string | null = null;

    @Column({
        type: 'text',
        nullable: true
    })
    description: string | null = null;

    @Column({
        type: 'enum',
        enum: ProfileStatus,
        default: ProfileStatus.Offline
    })
    status!: ProfileStatus;

    @CreateDateColumn()
    createdAt!: string;

    @UpdateDateColumn()
    updatedAt!: string;

    @DeleteDateColumn()
    deletedAt!: string;

    @OneToOne(() => User, (user) => user.profile)
    user!: User;

    @Column()
    userId!: number;

    @Column({
        type: 'text',
        nullable: true,
        default: null
    })
    avatarUrl!: string;
    
}