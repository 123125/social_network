export enum ProfileGender {
    MALE = 'male',
    FEMALE = 'female',
};

export enum ProfileStatus {
    Online = 'online',
    Offline = 'offline',
};