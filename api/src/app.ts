import { json, urlencoded } from 'body-parser';
import express, { Express } from 'express';
import cors from 'cors';
import 'reflect-metadata';
// import multer from 'multer';

import { ConfigService } from './config/config.service';
import { TypeOrmService } from './database/type-orm.service';
import { ExceptionFilter } from './errors/exception.filter';
import { FriendRelationRoutes } from './friend-relation/friend-relation.routes';
import { LoggerService } from './logger/logger.service';
import { AuthMiddleware } from './middleware/auth.middleware';
import { HeadersMiddleware } from './middleware/headers.middleware';
import { ProfilesRoutes } from './profiles/profiles.routes';
import { UsersRoutes } from './users/users.routes';

export const enum RoutesPath {
    USERS = '/users',
    PROFILES = '/profiles',
    FRIENDS = '/friends',
    UPLOADS = 'uploads',
}

export class App {
    public app: Express | undefined;
    private port: number | undefined;

    private _loggerService: LoggerService | undefined;
    private _configService: ConfigService | undefined;
    private _databaseService: TypeOrmService | undefined;
    private _exceptionFilter: ExceptionFilter | undefined;

    // getters:
    get loggerService(): LoggerService {
        if (this._loggerService === undefined) {
            throw Error('[App] Error: loggerService is not initialized!');
        }
        return this._loggerService;
    }
    get configService(): ConfigService {
        if (this._configService === undefined) {
            throw Error('[App] Error: configService is not initialized!');
        }
        return this._configService;
    }
    get databaseService(): TypeOrmService {
        if (this._databaseService === undefined) {
            throw Error('[App] Error: databaseService is not initialized!');
        }
        return this._databaseService;
    }
    get exceptionFilter(): ExceptionFilter {
        if (this._exceptionFilter === undefined) {
            throw Error('[App] Error: exceptionFilter is not initialized!');
        }
        return this._exceptionFilter;
    }

    constructor() {
        this._loggerService = new LoggerService();
        this._configService = new ConfigService(this.loggerService);
        this._databaseService = new TypeOrmService(this.configService, this.loggerService);
        this._exceptionFilter = new ExceptionFilter(this.loggerService);

        this.app = express();
        this.port = Number(this.configService.get('PORT'));
    }

    private useRoutes(app: Express): void {
        app.use(RoutesPath.USERS, new UsersRoutes(
            this.databaseService,
            this.configService,
            this.loggerService
        ).router);
        app.use(RoutesPath.PROFILES, new ProfilesRoutes(
            this.databaseService,
            this.configService,
            this.loggerService
        ).router);
        app.use(RoutesPath.FRIENDS, new FriendRelationRoutes(
            this.databaseService,
            this.loggerService
        ).router);
    }

    private async useMiddleware(app: Express): Promise<void> {
        // cors
        app.use(cors());
        // body parser
        app.use(urlencoded({ extended: true }));
        app.use(json());

        app.use('', express.static(RoutesPath.UPLOADS))

        // add headers
        const headersMiddleware = new HeadersMiddleware();
        app.use(headersMiddleware.execute.bind(headersMiddleware));

        // check jwt
        const authMiddleware = new AuthMiddleware(this.configService);
        app.use(authMiddleware.execute.bind(authMiddleware));
    }

    private useExceptionFilter(app: Express): void {
        app.use(this.exceptionFilter.catch.bind(this.exceptionFilter));
    }

    public async init(): Promise<void> {
        if (this.app === undefined) {
            throw Error('[App] Error: app is not initialized!');
        }
        try {
            this.loggerService.info(`[App] Server run on port ${this.port}`);
            this.useMiddleware(this.app);
            this.useRoutes(this.app);
            this.useExceptionFilter(this.app);
            await this.databaseService.connect();
            this.app.listen(this.port);
        } catch (error) {
            this.loggerService.error(`[App] ${error}`)
            throw error;
        }
    }
}

