import { BaseRoute } from '../common/base.route';
import { ConfigService } from '../config/config.service';
import { TypeOrmService } from '../database/type-orm.service';
import { ILogger } from '../logger/logger.interface';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

enum UsersRoutesPath {
    LOGIN = '/login',
    REGISTER = '/register',
    REQUEST_RESET_PASSWORD = '/request-reset-password',
    RESET_PASSWORD = '/reset-password',
}

export class UsersRoutes extends BaseRoute {
    constructor(
        private readonly typeOrmService: TypeOrmService,
        private readonly configService: ConfigService,
        logger: ILogger
    ) {
        super(logger);
        const usersService = new UsersService(this.typeOrmService, this.configService);
        const controller = new UsersController(usersService, logger);
        this.routes = [
            {
                method: 'post',
                path: UsersRoutesPath.LOGIN,
                handler: controller.login.bind(controller),
            },
            {
                method: 'post',
                path: UsersRoutesPath.REGISTER,
                handler: controller.register.bind(controller),
            },
            {
                method: 'post',
                path: UsersRoutesPath.REQUEST_RESET_PASSWORD,
                handler: controller.resetPasswordRequest.bind(controller),
            },
            {
                method: 'post',
                path: UsersRoutesPath.RESET_PASSWORD,
                handler: controller.resetPassword.bind(controller),
            },
        ];
        this.bindRoutes(this.routes);
    }
}