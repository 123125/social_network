import { Request, Response, NextFunction } from 'express';
import { HttpErrorMessage } from '../errors/http-error-message.enum';
import { HttpError } from '../errors/http-error.class';
import { ILogger } from '../logger/logger.interface';
import { UsersService } from './users.service';

export class UsersController {
    constructor(
        private readonly usersService: UsersService,
        private readonly logger: ILogger
    ) {}

    public async login(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<void> {
        try {
            const user = await this.usersService.findUserByLoginAndPassword(req.body);
            if (user === undefined) {
                return next(new HttpError(404, HttpErrorMessage.NOT_FOUND, '[UsersController] [login]'));
            }
            const token = await this.usersService.createJWT(user);
            res.status(200).json({ token });
        } catch (error) {
            this.logger.error(`[UsersController] [login] ${error}`);
            next(error);
        }
    }

    public async register(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<void> {
        try {
            const user = await this.usersService.createUser(req.body);
            const token = await this.usersService.createJWT(user);
            res.status(200).json({ token });
        } catch (error) {
            this.logger.error(`[UsersController] [register] ${error}`);
            next(error);
        }
    }

    public async resetPasswordRequest(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<void> {
        try {
            const { email } = req.body;
            const { origin } = req.headers;
            const user = await this.usersService.findUserByEmail(email);

            if (user === undefined) {
                return next(new HttpError(404, HttpErrorMessage.NOT_FOUND, '[UserController] [resetPasswordRequest]'));
            }

            await this.usersService.generateUserPasswordReset(user);
            if (origin !== undefined) {
                const message = this.usersService.sendResetPasswordMail(origin, user);
                res.status(200).json({ message });
                res.status(200);
                return;
            }
            next(new HttpError(400, 'Error: There is no origin header.'))

        } catch (error) {
            this.logger.error(`[UserController] [resetPasswordRequest] ${error}`);
            next(error);
        }
    }

    public async resetPassword(
        req: Request,
        res: Response,
        next: NextFunction,
    ): Promise<void> {
        try {
            const { email, token, password } = req.body;
            const user = await this.usersService.findUserByEmail(email);

            if (user === undefined) {
                return next(new HttpError(404, HttpErrorMessage.NOT_FOUND, '[UserController] [resetPassword]'));
            }

            const isTokenValid = user.resetPasswordToken !== null && user.resetPasswordToken === token;
            const isTokenNotExpired = user.resetPasswordExpired  !== null && user.resetPasswordExpired > Date.now();

            if (!isTokenValid || !isTokenNotExpired) {
                return next(new HttpError(400, HttpErrorMessage.BAD_REQUEST, '[UserController] [resetPassword]'));
            }

            const message = await this.usersService.resetPassword(password, user);

            res.status(200).json({ message });

        } catch (error) {
            this.logger.error(`[UserController] [resetPassword] ${error}`);
            next(error);
        }
    }
}