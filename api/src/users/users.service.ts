import { Repository } from 'typeorm';
import { sign } from 'jsonwebtoken';
import { TypeOrmService } from '../database/type-orm.service';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './user.entity';
import { LoginUserDto } from './dto/login-user.dto';
import { ConfigService } from '../config/config.service';
import { randomBytes } from 'crypto';
import { generateMailOptins, generateMailTransportOptions, resetPasswordTokenDuration, sendResetPasswordMail } from './helpers/send-mail';


export class UsersService {
    private userRepository: Repository<User>;

    constructor(
        private readonly databaseService: TypeOrmService,
        private readonly configService: ConfigService
    ) {
        this.userRepository = this.databaseService.appDataSource.getRepository(User);
    }

    public async findUserByLoginAndPassword(loginUserDto: LoginUserDto): Promise<User | undefined> {
        const { login, password } = loginUserDto;

        const foundUser = await this.userRepository.manager.findOneBy<User>(User, { login });
        const isPasswordCorrect = foundUser !== null
            ? foundUser.validatePassword(password, foundUser.password)
            : false;

        if (foundUser === null || !isPasswordCorrect) {
            return undefined;
        }
        return foundUser;
    }

    public async generateUserPasswordReset(user: User): Promise<void> {
        user.resetPasswordToken = randomBytes(20).toString('hex');
        user.resetPasswordExpired = Date.now() + resetPasswordTokenDuration;

        await this.userRepository.manager.save(user);
    }

    public sendResetPasswordMail(origin: string, user: User): string {
        const transporterOptions = generateMailTransportOptions(this.configService);
        const mailOptions = generateMailOptins(origin, user);
        sendResetPasswordMail(mailOptions, transporterOptions);
        return 'A reset email has been sent to ' + user.email + '.'
    }

    public async resetPassword(password: string, user: User): Promise<string> {
        await user.setPassword(password);
        this.userRepository.save(user);
        return 'Password is reset successfully';
    }

    public async findUserByEmail(email: string): Promise<User | undefined> {
        const user = await this.userRepository.manager.findOneBy<User>(User, { email }) ?? undefined;
        return user;
    }

    public async createUser(createUserDto: CreateUserDto): Promise<User> {
        const { email, login, password } = createUserDto;

        const newUser = new User();
        newUser.email = email;
        newUser.login = login;
        newUser.createdAt = new Date().toISOString();
        newUser.updatedAt = new Date().toISOString();

        await newUser.setPassword(password);
        return this.userRepository.save(newUser);
    }

    public async addProfileIdToUser(userId: string, profileId: number): Promise<User> {
        const foundUser = await this.userRepository.manager.findOneBy<User>(
            User,
            { id: Number(userId) },
        ) ?? undefined;
        if (foundUser === undefined) {
            throw Error('[UsersService] [addProfileIdToUser] user is not found');
        }
        foundUser.profileId = profileId;
        return this.userRepository.save(foundUser);
    }

    public async createJWT(user: User): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const { id, email, login, profileId } = user;
            sign(
                { id, email, login, profileId },
                this.configService.get('JWT_SECRET'),
                {
                    algorithm: 'HS256'
                },
                (err: Error | null, token: string | undefined) => {
                    if (err !== null) {
                        reject(err);
                    }
                    if (token === undefined) {
                        throw Error('[UsersService] [createJWT] token is undefined');
                    }
                    resolve(token);
                }
            )
        });
    }

}