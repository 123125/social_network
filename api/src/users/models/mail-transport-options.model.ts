export type MailTransportOptions = {
    service: string,
    auth: {
        user: string,
        pass: string,
    },
};