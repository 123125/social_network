import { IsNotEmpty, IsString, Length } from 'class-validator';

export class LoginUserDto {
    @IsString()
    @IsNotEmpty()
    @Length(4, 20)
    login!: string;

    @IsString()
    @IsNotEmpty()
    @Length(4, 20)
    password!: string;
}