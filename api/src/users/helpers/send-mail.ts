import * as nodemailer from 'nodemailer';
import { SentMessageInfo } from 'nodemailer';
import { MailOptions } from 'nodemailer/lib/sendmail-transport';
import { ConfigService } from '../../config/config.service';
import { MailTransportOptions } from '../models/mail-transport-options.model';
import { User } from '../user.entity';


export const resetPasswordTokenDuration: number = 60 * 60 * 1000; // 1 hour

export const sendResetPasswordMail = (
    mailOptions: MailOptions,
    transporterOptions: MailTransportOptions,
): void => {
    const transporter = nodemailer.createTransport(transporterOptions);
    transporter.sendMail(mailOptions, (error: Error | null, info: SentMessageInfo) => {
        // TODO: add logger instead of console logs
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
};

export const generateMailTransportOptions = (
    configService: ConfigService
): MailTransportOptions => ({
    service: configService.get('NODEMAILER_SERVICE'),
    auth: {
        user: configService.get('NODEMAILER_EMAIL'),
        pass: configService.get('NODEMAILER_PASSWORD'),
    },
});

export const generateMailOptins = (
    origin: string,
    user: User,
): SentMessageInfo => {
    const link = 'http://' + origin + '/auth/reset-password/' + user.resetPasswordToken + '?email=' + user.email ;
    return {
        from: 'Social Network',
        to: user.email,
        subject: 'Reset Password Request',
        text: `Hi ${user.login} \n
            Please click on the following link ${link} to reset your password. \n\n 
            If you did not request this, please ignore this email and your password will remain unchanged.\n
        `,
    }
};