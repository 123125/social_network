import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { compare, hash, genSalt } from 'bcryptjs';
import { Profile } from '../profiles/profile.entity';
@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({
        unique: true,
    })
    email!: string;

    @Column({
        unique: true,
    })
    login!: string;

    @Column()
    password!: string;

    @Column('date')
    createdAt!: string;

    @Column('date')
    updatedAt!: string;

    @OneToOne(() => Profile, (profile) => profile.user)
    profile!: Profile;

    @Column({
        type: 'integer',
        nullable: true,
        unique: true,
    })
    profileId: number | null = null;

    @Column({
        type: String,
        unique: true,
        nullable: true,
    })
    resetPasswordToken!: string | null;

    @Column({
        type: 'bigint',
        nullable: true,
    })
    resetPasswordExpired!: number | null;

    public async setPassword(password: string): Promise<void> {
        try {
            this.password = await hash(password, await genSalt());
        } catch(error: unknown) {
            throw new Error(`Error: can not set password -> ${error}`);
        }
    }

    public async validatePassword(password: string, hash: string): Promise<boolean> {
        return compare(password, hash)
    }
}