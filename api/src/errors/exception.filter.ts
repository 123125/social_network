import { Request, Response, NextFunction } from 'express';
import { ILogger } from '../logger/logger.interface';
import { IExceptionFilter } from './exception.filter.interface';
import { HttpError, httpErrorName } from './http-error.class';

const isHttpError = (error: Error): error is HttpError => {
    const httpError = error as HttpError;
    if (
        httpError.statusCode !== undefined &&
        httpError.name === httpErrorName
    ) {
        return true;
    }
    return false;
}

export class ExceptionFilter implements IExceptionFilter {
    constructor(
        private readonly logger: ILogger
    ) {}

    catch(error: Error | HttpError, _req: Request, res: Response, _next: NextFunction): void {
        // TODO: investigate why error instanceof HttpError === false.
        // (error instanceof HttpError) returns false even if the error is HttpError instance,
        // so we use custom guard to check it
        if (isHttpError(error)) {
            this.logger.error(`${(error as HttpError).context} Error - ${(error as HttpError).statusCode}: ${error.message}`);
            res.status((error as HttpError).statusCode).json({ error: error.message });
        } else {
            this.logger.error(`Error: ${error.message}`);
            res.status(500).json({ error: error.message });
        }
    }
}