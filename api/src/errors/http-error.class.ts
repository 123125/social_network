
export const httpErrorName = 'HttpError';
export class HttpError extends Error {
    statusCode: number;
	context?: string;

	constructor(statusCode: number, message: string, context?: string) {
		super(message);
		this.name = httpErrorName;
		this.statusCode = statusCode;
		this.message = message;
		this.context = context;
	}
}