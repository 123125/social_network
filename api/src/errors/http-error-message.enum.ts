export enum HttpErrorMessage {
    NOT_AUTHORIZED = 'Not authorized',
    BAD_REQUEST = 'Bad request',
    NOT_FOUND = 'Not found',
    PROFILE_ALREADY_EXIST = 'Profile has already existed',
}