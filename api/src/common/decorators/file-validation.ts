import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';

type IsFileOptions = {
    mime: ('image/jpg' | 'image/png' | 'image/jpeg')[];
}

export const IsFile = (options: IsFileOptions, validationOptions?: ValidationOptions) =>{
    return  (object: Object, propertyName: string) => {
        return registerDecorator({
            name: 'isFile',
            target: object.constructor,
            propertyName: propertyName,
            constraints: [],
            options: validationOptions,
            validator: {
                validate(value: any) {
                    if (value?.mimetype && (options?.mime ?? []).includes(value?.mimetype)) {
                        return true;
                    }                        
                    return false;
                },
            }
        });
    }
}