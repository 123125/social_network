import { Router } from 'express';
import multer from 'multer';
// import multer from 'multer';
import { ILogger } from '../logger/logger.interface';
import { IRoute } from './route.interface';

export abstract class BaseRoute {
    private readonly _router: Router;
    private _routes: IRoute[] | undefined;

    get router(): Router {
        return this._router;
    }

    get routes(): IRoute[] {
        if (this._routes === undefined) {
            throw Error('[BaseRoute] routes are not set')
        }
        return this._routes;
    }

    set routes(routes: IRoute[]) {
        this._routes = routes;
    }

    constructor(readonly logger: ILogger) {
        this._router = Router();
    }

    // TODO: refact middleware logic
    protected bindRoutes(routes: IRoute[]): void {
        for (const route of routes) {
            this.logger.info(`[bindRoutes] [${route.method}] [${route.path}]`);
            const handler = route.handler.bind(this);
            if (route.middleware) {
                const storage = multer.diskStorage({
                    destination: function (_req, _file, cb) {
                        cb(null, 'uploads/profile-photos');
                    },                    
                    filename: function (_req, file, cb) {
                      cb(null, file.fieldname + '-' + Date.now() + '.' +  file.mimetype.split('/')[1])
                    }                     
                  });
                this._router[route.method](route.path, multer({storage}).single("photo"), handler);
            } else {
                this._router[route.method](route.path, handler);
            }
        }
    }
}