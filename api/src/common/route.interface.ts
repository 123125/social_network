import { Request, Response, NextFunction, Router } from 'express';
import { IMiddleware } from '../middleware/middleware.interface';

export interface IRoute {
    path: string,
    handler: (req: Request, res: Response, next: NextFunction) => void,
    method: keyof Pick<Router, 'get' | 'post' | 'patch' | 'put' | 'delete'>,
    middleware?: IMiddleware,
};