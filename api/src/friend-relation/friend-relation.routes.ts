import { BaseRoute } from '../common/base.route';
import { TypeOrmService } from '../database/type-orm.service';
import { ILogger } from '../logger/logger.interface';
import { ProfilesService } from '../profiles/profiles.service';
import { FriendRelationController } from './friend-relation.controller';
import { FriendRelationService } from './friend-relation.service';

enum FriendRelationPath {
    ADD = '/add',
    DELETE = '/delete/:id',
    LIST = '/list',
    YOUR_REQUESTS = '/your-requests',
    FRIEND_REQUESTS = '/friends-requests',
    ACTIVATE_FRIEND = '/activate/:id',
    FRIEND_BY_ID = '/:id',
}
export class FriendRelationRoutes extends BaseRoute {
    constructor(
        private readonly databaseService: TypeOrmService,
        logger: ILogger
    ) {
        super(logger);
        const friendRelationService = new FriendRelationService(this.databaseService);
        const profilesService = new ProfilesService(this.databaseService, friendRelationService);
        const controller = new FriendRelationController(friendRelationService, profilesService, logger);

        this.routes = [
            {
                path: FriendRelationPath.ADD,
                method: 'post',
                handler: controller.addFriendRelation.bind(controller),
            },
            {
                path: FriendRelationPath.DELETE,
                method: 'delete',
                handler: controller.deleteFriendRelation.bind(controller),
            },
            {
                path: FriendRelationPath.LIST,
                method: 'get',
                handler: controller.getAllFriends.bind(controller),
            },
            {
                path: FriendRelationPath.YOUR_REQUESTS,
                method: 'get',
                handler: controller.getYourRequests.bind(controller),
            },
            {
                path: FriendRelationPath.FRIEND_REQUESTS,
                method: 'get',
                handler: controller.getFriendsRequests.bind(controller),
            },
            {
                path: FriendRelationPath.FRIEND_BY_ID,
                method: 'get',
                handler: controller.getFriendRelation.bind(controller),
            },
            {
                path: FriendRelationPath.ACTIVATE_FRIEND,
                method: 'put',
                handler: controller.activateFriendRelation.bind(controller),
            },
        ];
        this.bindRoutes(this.routes);
    }
}