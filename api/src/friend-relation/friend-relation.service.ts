import { Brackets, DeleteResult, Repository } from 'typeorm';
import { TypeOrmService } from '../database/type-orm.service';
import { Profile } from '../profiles/profile.entity';
import { FriendRelation } from './friend-relation.entity';

export class FriendRelationService {
    private friendsRelationRepository: Repository<FriendRelation>;

    constructor(private readonly databaseService: TypeOrmService) {
        this.friendsRelationRepository = this.databaseService.appDataSource.getRepository(FriendRelation);
    }

    /**
     * Creates a new friend relation
     * @param profile profile of the current user
     * @param friend profile of the friend which the current user invited
     */
    public async addFriendRelation(profile: Profile, friend: Profile): Promise<FriendRelation> {
        const foundFriendsRelation = await this.friendsRelationRepository.manager.findOneBy(FriendRelation, {
                profileId: profile.id,
                friendId: friend.id,
        }) ?? undefined;
        if (foundFriendsRelation !== undefined) {
            throw Error('[FriendRelationService] [addFriendRelation] This friend relation is already exsisted')
        }
        const friendsRelation = new FriendRelation();
        friendsRelation.profile = profile;
        friendsRelation.friend = friend;
        friendsRelation.isInvited = true;
        return this.friendsRelationRepository.manager.save(friendsRelation);
    }

    /**
     * Gets all the friends' relations of the current user which are accepted
     * @param profileId profile id of the current user
     */
    public async getAllFriends(profileId: string): Promise<FriendRelation[]> {
        return this.friendsRelationRepository.createQueryBuilder('friendRelation')
            .innerJoinAndSelect('friendRelation.profile', 'profile')
            .where('friendRelation.accepted = true')
            .andWhere(new Brackets(qb => {
                qb.where('friendRelation.profileId = :profileId OR friendRelation.friendId = :profileId');
            }))
            .setParameters({ profileId })
            .getMany();
    }

    /**
     * Gets all the friends' requests of the current user which are not accepted 
     * @param profileId profile id of the current user
     */
    public async getYourFriendsRequests(profileId: string): Promise<FriendRelation[]> {
        return this.friendsRelationRepository.createQueryBuilder('friendRelation')
            .innerJoinAndSelect('friendRelation.profile', 'profile')
            .innerJoinAndSelect('friendRelation.friend', 'friend')
            .where('friendRelation.accepted = false')
            .andWhere('friendRelation.profileId = :profileId')
            .setParameters({ profileId })
            .getMany();
    }

    /**
     * Gets all the friends' requests of the friends which are not accepted
     * @param profileId profile id of the current user
     */
    public async getFriendsRequests(profileId: string): Promise<FriendRelation[]> {
        return this.friendsRelationRepository.createQueryBuilder('friendRelation')
            .innerJoinAndSelect('friendRelation.profile', 'profile')
            .innerJoinAndSelect('friendRelation.friend', 'friend')
            .where('friendRelation.accepted = false')
            .andWhere('friendRelation.friendId = :profileId')
            .setParameters({ profileId })
            .getMany();
    }

    /**
     * Deletes friend relation by its id
     * @param relationId id of the friend relation
     */
    public async deleteFriendRelation(
        relationId: string
    ): Promise<DeleteResult> {
        return this.friendsRelationRepository.manager.delete(FriendRelation, { id: relationId })
    }

    /**
     * Gets a friend relation by its id
     * @param relationId id of the friend relation
     */
    public async getFriendRelation(relationId: string): Promise<FriendRelation | undefined> {
        const friendRelation = await this.friendsRelationRepository.manager.findOneById(FriendRelation, relationId);
        return (friendRelation !== null) ? friendRelation : undefined; 
    }

    /**
     * Sets accepted field as true
     * @param friendRelation friend relation to make update
     */
    public async activateFriendRelation(friendRelation: FriendRelation): Promise<FriendRelation> {
        friendRelation.accepted = true;
        friendRelation.isInvited = false;
        return this.friendsRelationRepository.manager.save(friendRelation);
    }
}