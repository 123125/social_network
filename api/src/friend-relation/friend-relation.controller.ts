import { Request, Response, NextFunction } from 'express';
import { HttpErrorMessage } from '../errors/http-error-message.enum';
import { HttpError } from '../errors/http-error.class';
import { ILogger } from '../logger/logger.interface';
import { ProfilesService } from '../profiles/profiles.service';
import { FriendRelationService } from './friend-relation.service';

export class FriendRelationController {
    constructor(
        private readonly friendRelationService: FriendRelationService,
        private readonly profilesService: ProfilesService,
        private readonly logger: ILogger
    ) {}

    /**
     * Creates a new friend relation
     */
    public async addFriendRelation(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        const { profileId, friendId } = req.body;
        if (profileId === undefined || friendId === undefined) {
            return next(new HttpError(400, HttpErrorMessage.BAD_REQUEST));
        }
        try {
            const profile = await this.profilesService.getProfileById(profileId);
            const friend = await this.profilesService.getProfileById(friendId);
            if (profile === undefined || friend === undefined) {
                return next(new HttpError(404, HttpErrorMessage.NOT_FOUND));
            }

            const friendRelation = await this.friendRelationService.addFriendRelation(profile, friend);
            res.status(201).json(friendRelation);
        } catch (error) {
            this.logger.error(`[FriendRelationController] [addRelation] ${error}`);
            return next(error);
        }
    }

    /**
     * Deletes friend relation by its id
     */
    public async deleteFriendRelation(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        const id = req.params['id'];
        if (id === undefined) {
            return next(new HttpError(400, HttpErrorMessage.BAD_REQUEST));
        }
        try {
            const result = await this.friendRelationService.deleteFriendRelation(id);
            if (result.affected === 0) {
                return next(new HttpError(404, HttpErrorMessage.NOT_FOUND));
            }
            res.status(200).send();
        } catch (error) {
            this.logger.error(`[FriendRelationController] [deleteFriendRelation] ${error}`);
            return next(error);
        }
    }

    /**
     * Gets all the friends' relations of the current user which are accepted
     */
    public async getAllFriends(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        const { profileId } = req.query;
        if (profileId === undefined) {
            return next(new HttpError(400, HttpErrorMessage.BAD_REQUEST));
        }
        try {
            const friends = await this.friendRelationService.getAllFriends(profileId?.toString());
            res.status(200).json({
                list: friends
            });
        } catch (error) {
            this.logger.error(`[FriendRelationController] [getFriendRelationList] ${error}`);
            return next(error);
        }
    }

    /**
     * Gets all the friends' requests of the current user which are not accepted 
     */
    public async getYourRequests(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        const { profileId } = req.query;
        if (profileId === undefined) {
            return next(new HttpError(400, HttpErrorMessage.BAD_REQUEST));
        }
        try {
            const friends = await this.friendRelationService.getYourFriendsRequests(profileId?.toString());
            res.status(200).json({
                list: friends
            });
        } catch (error) {
            this.logger.error(`[FriendRelationController] [getFriendRelationList] ${error}`);
            return next(error);
        }
    }

    /**
     * Gets all the friends' requests of the friends which are not accepted
     */
    public async getFriendsRequests(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        const { profileId } = req.query;
        if (profileId === undefined) {
            return next(new HttpError(400, HttpErrorMessage.BAD_REQUEST));
        }
        try {
            const friends = await this.friendRelationService.getFriendsRequests(profileId?.toString());
            res.status(200).json({
                list: friends
            });
        } catch (error) {
            this.logger.error(`[FriendRelationController] [getFriendRelationList] ${error}`);
            return next(error);
        }
    }

    /**
     * Gets a friend relation by id
     */
     public async getFriendRelation(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        const { id } = req.params;
        if (id === undefined) {
            return next(new HttpError(404, HttpErrorMessage.NOT_FOUND))
        }
        try {
            const relation = await this.friendRelationService.getFriendRelation(id);
            res.status(200).json(relation);
        } catch (error) {
            this.logger.error(`[FriendRelationController] [getFriendRelation] ${error}`);
            return next(error);
        }
    }

    /**
     * Sets accepted field as true
     */
    public async activateFriendRelation(
        req: Request, res: Response, next: NextFunction
    ): Promise<void> {
        const friendRelationId = req.params['id'];
        if (friendRelationId === undefined) {
            return next(new HttpError(400, HttpErrorMessage.BAD_REQUEST));
        }
        try {
            const friendRelation = await this.friendRelationService.getFriendRelation(friendRelationId);
            if (friendRelation === undefined) {
                return next(new HttpError(404, HttpErrorMessage.NOT_FOUND));
            }

            await this.friendRelationService.activateFriendRelation(friendRelation);
            res.status(200).json(friendRelation);
        } catch (error) {
            this.logger.error(`[FriendRelationController] [getFriendRelationList] ${error}`);
            return next(error);
        }
    }
}