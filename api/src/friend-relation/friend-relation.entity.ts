import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Profile } from '../profiles/profile.entity';

@Entity()
export class FriendRelation {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({
        default: false
    })
    accepted: boolean = false;

    @UpdateDateColumn()
    updatedAt!: string;

    @CreateDateColumn()
    createdAt!: string;

    @Column()
    friendId!: number;

    @Column()
    profileId!: number;

    @Column({
        default: true
    })
    isInvited: boolean = true;

    @ManyToOne(() => Profile, (profile) => profile)
    friend!: Profile;

    @ManyToOne(() => Profile, (profile) => profile)
    profile!: Profile;
}